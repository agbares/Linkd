//
//  NewMessageNavigationViewController.swift
//  Linkd
//
//  Created by Antonio Bares on 5/21/16.
//  Copyright © 2016 jav. All rights reserved.
//

import UIKit

class NewMessageNavigationViewController: UINavigationController {
    
    var senderId : String?
    var senderName : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}