//
//  ChangePasswordTableViewController.swift
//  Linkd
//
//  Created by Antonio Bares on 5/24/16.
//  Copyright © 2016 jav. All rights reserved.
//

import UIKit
import Firebase

class ChangePasswordTableViewController: UITableViewController, UITextFieldDelegate {
    
    // Outlets
    @IBOutlet var textFieldCurrentPassword: UITextField!
    @IBOutlet var textFieldNewPassword: UITextField!
    @IBOutlet var textFieldNewPasswordConfirm: UITextField!
    
    @IBOutlet var navigationItemCancel: UIBarButtonItem!
    @IBOutlet var navigationItemDone: UIBarButtonItem!
    
    // User credentials
    var userEmail: String!
    var UserNewPassword: String!
    
    // Keep track of variables
    var isCorrectPassword = false
    var isMatch = false
    var isValidPassword = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        // Assign delegates for return keys
        self.textFieldCurrentPassword.delegate = self
        self.textFieldNewPassword.delegate = self
        self.textFieldNewPasswordConfirm.delegate = self
        
        // Disable done button
        self.navigationItemDone.enabled = false
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.grayColor(), NSFontAttributeName: UIFont(name: "CaviarDreams", size: 20)!], forState: UIControlState.Normal)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // TextField Actions
    
    @IBAction func textFieldCurrentPasswordEditEnd(sender: AnyObject) {
        if let user = FIRAuth.auth()?.currentUser {
            // Get current user's email
            self.userEmail = user.email
            

            FIRAuth.auth()?.signInWithEmail(self.userEmail, password: self.textFieldCurrentPassword.text!) { (user, error) in
                if error == nil {
                    self.isCorrectPassword = true
                    
                    if self.isCorrectPassword == true && self.isValidPassword == true && self.isMatch == true {
                        self.navigationItemDone.enabled = true
                        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "CaviarDreams", size: 20)!], forState: UIControlState.Normal)
                    }
                    
                } else {
                    print("Incorrect Password")
                    // Show error to user
                    self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(red: 0.8196, green: 0.2549, blue: 0.1294, alpha: 1.0), NSFontAttributeName: UIFont(name: "CaviarDreams-Bold", size: 18)!]
                    self.navigationItem.title = "Incorrect Password"
                    self.navigationItemDone.enabled = false
                    
                    // Delay for 5 seconds before reverting back to original navigation bar
                    let triggerTime = (Int64(NSEC_PER_SEC) * 2)
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, triggerTime), dispatch_get_main_queue(), { () -> Void in
                        
                        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "CaviarDreams-Bold", size: 20)!]
                        self.navigationItem.title = "PASSWORD"
                    })
                    
                    self.isCorrectPassword = false
                    self.navigationItemDone.enabled = false
                    self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.grayColor(), NSFontAttributeName: UIFont(name: "CaviarDreams", size: 20)!], forState: UIControlState.Normal)
                }
                
            }
            
        } else {
            print("User not logged in")
        }
        
    }
    
    @IBAction func textFieldNewPasswordEditEnd(sender: AnyObject) {
        
        if textFieldNewPassword.text!.characters.count < 7 {
            
            // Show error to user
            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(red: 0.8196, green: 0.2549, blue: 0.1294, alpha: 1.0), NSFontAttributeName: UIFont(name: "CaviarDreams-Bold", size: 12)!]
             self.navigationItem.title = "Pasword must be at least 7 characters"
            self.navigationItemDone.enabled = false
            
            // Delay for 5 seconds before reverting back to original navigation bar
            let triggerTime = (Int64(NSEC_PER_SEC) * 2)
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, triggerTime), dispatch_get_main_queue(), { () -> Void in
                
                self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "CaviarDreams-Bold", size: 20)!]
                self.navigationItem.title = "PASSWORD"
            
            })
            
           
        } else if textFieldNewPassword.text!.characters.count > 7 && isValidPassword == false {
            isValidPassword = true
        } else if isCorrectPassword == true && isValidPassword == true && isMatch == true {
            self.navigationItemDone.enabled = true
            self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "CaviarDreams", size: 20)!], forState: UIControlState.Normal)
        }
        
    }
    
    @IBAction func textFieldnewPasswordConfirmEditEnd(sender: AnyObject) {
        if textFieldNewPassword.text! != textFieldNewPasswordConfirm.text! {
            // Show error to user
            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(red: 0.8196, green: 0.2549, blue: 0.1294, alpha: 1.0), NSFontAttributeName: UIFont(name: "CaviarDreams-Bold", size: 18)!]
            self.navigationItem.title = "Paswords do not match"
            self.navigationItemDone.enabled = false
            self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.grayColor(), NSFontAttributeName: UIFont(name: "CaviarDreams", size: 20)!], forState: UIControlState.Normal)
            
            // Delay for 5 seconds before reverting back to original navigation bar
            let triggerTime = (Int64(NSEC_PER_SEC) * 2)
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, triggerTime), dispatch_get_main_queue(), { () -> Void in
                
                self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "CaviarDreams-Bold", size: 20)!]
                self.navigationItem.title = "PASSWORD"
                
            })
        } else if textFieldNewPassword.text! == textFieldNewPasswordConfirm.text! && isMatch == false {
            isMatch = true
        } else if isCorrectPassword == true && isValidPassword == true && isMatch == true {
            self.navigationItemDone.enabled = true
            self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "CaviarDreams", size: 20)!], forState: UIControlState.Normal)
        }
    }
    
    
    // Make sure we disable the done button each time a user begins typing in a textField
    @IBAction func textFieldCurrentPasswordEditBegin(sender: AnyObject) {
        self.navigationItemDone.enabled = false
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.grayColor(), NSFontAttributeName: UIFont(name: "CaviarDreams", size: 20)!], forState: UIControlState.Normal)
    }
    
    @IBAction func textFieldNewPasswordEditBegin(sender: AnyObject) {
        self.navigationItemDone.enabled = false
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.grayColor(), NSFontAttributeName: UIFont(name: "CaviarDreams", size: 20)!], forState: UIControlState.Normal)
    }
    
    @IBAction func textFieldNewPasswordConfirmEditBegin(sender: AnyObject) {
        self.navigationItemDone.enabled = false
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.grayColor(), NSFontAttributeName: UIFont(name: "CaviarDreams", size: 20)!], forState: UIControlState.Normal)
    }
    
    // Make sure when the user clicks a table cell that it goes to the textField
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0 {
            textFieldCurrentPassword.becomeFirstResponder()
        } else {
            if indexPath.row == 0 {
                textFieldNewPassword.becomeFirstResponder()
            } else {
                textFieldNewPasswordConfirm.becomeFirstResponder()
            }
        }
    }

    // Catch return keys
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == textFieldCurrentPassword {
            textFieldNewPassword.becomeFirstResponder()
        } else if textField == textFieldNewPassword {
            textFieldNewPasswordConfirm.becomeFirstResponder()
        } else if textField == textFieldNewPasswordConfirm {
            textFieldNewPasswordConfirm.resignFirstResponder()
        }
        return true
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "segueChangePassword" {
            self.dismissKeyboard()
            self.UserNewPassword = textFieldNewPassword.text!
            
        } else if segue.identifier == "segueCancelChangePassword" {
            self.dismissKeyboard()
        }
    }
 

}
