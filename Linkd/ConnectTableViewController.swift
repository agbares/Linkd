//
//  ConnectTableViewController.swift
//  Linkd
//
//  Created by Antonio Bares on 5/12/16.
//  Copyright © 2016 jav. All rights reserved.
//

import UIKit
import Firebase
import ImageIO

class ConnectTableViewController: UITableViewController {
    
    // Current user data
    var userId: String!
    var interest1: String!
    var interest2: String!
    
    // PLACEHOLDER. IMPLEMENT A MORE EFFICIENT CHECK
    var isFirst: Bool! = false
    
    var matchedUsers: [MatchedUser] = []
    
    // Database references
    var userInterestsRef: FIRDatabaseReference!
    var currentUserInterestsRef: FIRDatabaseReference!
    var interestsRatiosRef: FIRDatabaseReference!
    
    // Setup Firebae storage
    let storage = FIRStorage.storage()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        interestsRatiosRef = FIRDatabase.database().reference().child(Globals.CONSTANTS.INTERESTS_MATCH_PERCENTAGES_ROOT)
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
    }
    
    override func viewDidAppear(animated: Bool) {
        
        // We must get the current user and nest all database checks inside this statement
        if let user = FIRAuth.auth()?.currentUser {
            self.userId = user.uid
            print(self.userId)
            
            self.userInterestsRef = FIRDatabase.database().reference().child(Globals.CONSTANTS.USER_INTERESTS_ROOT)
            self.currentUserInterestsRef = userInterestsRef.child(self.userId)
            
            // Let's grab the current user's interests
            self.currentUserInterestsRef.queryLimitedToLast(2).observeEventType(.Value, withBlock: { (snapshot) -> Void in
                
                // Check if a user has set their interests
                if snapshot.exists() == false {
                    print("Interests not set!")
                    
                } else {
                    
                    if self.isFirst == false {
                        self.interest1 = snapshot.childSnapshotForPath("interest-1").value?.valueForKey("name") as! String
                        self.interest2 = snapshot.childSnapshotForPath("interest-2").value?.valueForKey("name") as! String
                    } else {
                        self.isFirst = false
                    }
                    
                    print("Current User -> Interest1: \(self.interest1)\t\tInterest2: \(self.interest2)\n")
                    
                    // Let's grab the other users' interests
                    self.userInterestsRef.observeEventType(.ChildAdded, withBlock: { (snapshot) -> Void in
                        let key = snapshot.key
                        let matchedUserId = key
                        
                        print(snapshot)
                        
                        let tempInterest1 = snapshot.childSnapshotForPath("interest-1").value?.valueForKey("name") as! String
                        let tempInterest2 = snapshot.childSnapshotForPath("interest-2").value?.valueForKey("name") as! String
                        
                        // Setup reference for connections
                        let connectionsRef = FIRDatabase.database().reference().child(Globals.CONSTANTS.USER_CONNECTIONS_ROOT)
                        let matchedUserConnectionsRef = connectionsRef.child(matchedUserId)
                        
                        // Let's make sure the user's weren't already previously connected
                        matchedUserConnectionsRef.observeSingleEventOfType(.Value, withBlock: { (snapshot) in
                            
                            let isConnected = snapshot.hasChild(self.userId)
                            
                            if snapshot.key != self.userId && isConnected == false {

                                
                                print("User ID -> \(snapshot.key) -- Interest1: \(tempInterest1)\t\tInterest2: \(tempInterest2)\n")
                                
                                // -- Perform matchmaking algorithm
                                // Grab interests' matching ratios
                                
                                // Ratio 1
                                let interestRatioRef = self.interestsRatiosRef.child(self.interest1)
                                interestRatioRef.observeSingleEventOfType(.Value, withBlock: { (snapshot) -> Void in
                                    let tempRatio = snapshot.value?.valueForKey(tempInterest1) as! Float
                                    let ratio1 = tempRatio * 0.25
                                    //print("\(key)-> \(self.interest1)&\(tempInterest1)-> \(ratio1)")
                                    
                                    // Ratio 2
                                    let interestRatioRef = self.interestsRatiosRef.child(self.interest1)
                                    interestRatioRef.observeSingleEventOfType(.Value, withBlock: { (snapshot) -> Void in
                                        let tempRatio = snapshot.value?.valueForKey(tempInterest2) as! Float
                                        let ratio2 = tempRatio * 0.25
                                        //print("\(key)-> \(self.interest1)&\(tempInterest2)-> \(ratio2)")
                                        
                                        // Ratio 3
                                        let interestRatioRef = self.interestsRatiosRef.child(self.interest2)
                                        interestRatioRef.observeSingleEventOfType(.Value, withBlock: {(snapshot) -> Void in
                                            let tempRatio = snapshot.value?.valueForKey(tempInterest1) as! Float
                                            let ratio3 = tempRatio * 0.25
                                            //print("\(key)-> \(self.interest2)&\(tempInterest1)-> \(ratio3)")
                                            
                                            // Ratio 4
                                            let interestRatioRef = self.interestsRatiosRef.child(self.interest2)
                                            interestRatioRef.observeSingleEventOfType(.Value, withBlock: {(snapshot) -> Void in
                                                let tempRatio = snapshot.value?.valueForKey(tempInterest2) as! Float
                                                let ratio4 = tempRatio * 0.25
                                                //print("\(key)-> \(self.interest2)&\(tempInterest2)-> \(ratio4)")
                                                
                                                let finalRatio: Float = ratio1 + ratio2 + ratio3 + ratio4
                                                print("Final Ratio: \(key)--> \(finalRatio)")
                                                
                                                // Now let's check against the minimum matching ratio
                                                if finalRatio >= 0.4 {
                                                    print("MATCH!\n")
                                                    
                                                    
                                                    let userId = key
                                                    
                                                    let basicUserInfoRef = FIRDatabase.database().reference().child(Globals.CONSTANTS.USER_BASIC_INFO_ROOT)
                                                    let currentBasicUserInfoRef = basicUserInfoRef.child(userId)
                                                    
                                                    currentBasicUserInfoRef.observeSingleEventOfType(.Value, withBlock: {(snapshot) -> Void in
                                                        let name = snapshot.value?.valueForKey("Name") as! String
                                                        
                                                        print(userId)
                                                        print(name)
                                                        let newMatchedUser = MatchedUser(userId: userId, userName: name, matchedPercentage: finalRatio)
                                                        
                                                        print(self.matchedUsers.contains(newMatchedUser))
                                                        
                                                        if self.matchedUsers.contains(newMatchedUser) == false {
                                                            
                                                            // Let's make sure the user's listed are organized by their matching percentages
                                                            if self.matchedUsers.count == 0 && self.matchedUsers.contains(newMatchedUser) == false {       // First matched user should go on top
                                                                self.matchedUsers.append(newMatchedUser)
                                                                self.tableView.reloadData()
                                                                
                                                            } else if self.matchedUsers.count != 0 { // Next users need to be added to the array according
                                                                
                                                                var position = 0
                                                                var counter = 0
                                                                var userAdded = false
                                                                for currentUser in self.matchedUsers {
                                                                    if newMatchedUser.matchedPercentage > currentUser.matchedPercentage && self.matchedUsers.contains(newMatchedUser) == false {
                                                                        
                                                                        
                                                                        
                                                                        if self.matchedUsers.contains(newMatchedUser) == true {
                                                                            print("already exists")
                                                                            
                                                                        } else {
                                                                            position = counter
                                                                            counter += 1
                                                                            self.matchedUsers.insert(newMatchedUser, atIndex: position)
                                                                            print(self.matchedUsers.count)
                                                                            
                                                                            self.tableView.beginUpdates()
                                                                            self.tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: 0, inSection: 0)], withRowAnimation: .Automatic)
                                                                            self.tableView.reloadData()
                                                                            self.tableView.endUpdates()
                                                                            userAdded = true
                                                                        }
                                                                        
                                                                        
                                                                    }
                                                                    
                                                                }
                                                                
                                                                // If the matched user was never matched then we must add them in manually
                                                                if userAdded == false && self.matchedUsers.contains(newMatchedUser) == false {
                                                                    self.matchedUsers.append(newMatchedUser)
                                                                    print(self.matchedUsers.count)
                                                                    self.tableView.reloadData()
                                                                }
                                                                
                                                                
                                                            }
                                                            
                                                        }
                                                        

                                                        
                                                        
                                                        
                                                        
                                                    })
                                                    
                                                    
                                                    
                                                    
                                                } else {
                                                    print("Not a match\n")
                                                }
                                                
                                                
                                            })
                                            
                                        })
                                        
                                        
                                    })
                                    
                                })
                                
                                
                            }
                            
                        })
                        

                        
                        
                    })
                    
                }
                
                
                
            })
            
        }

    }

    override func viewDidDisappear(animated: Bool) {
        matchedUsers.removeAll()
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return matchedUsers.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let reuseIdentifier = "connects"
        
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier, forIndexPath: indexPath) as! ConnectTableViewCell
        // Configure the cell...
        
        let userId = self.matchedUsers[indexPath.row].userId
        let userName = self.matchedUsers[indexPath.row].userName
        let matchedPercentage = Int(self.matchedUsers[indexPath.row].matchedPercentage * 100)
        
        // Let's grab the user's profile picture
        
        var userProfilePicture: UIImage!
        
        // Get the local device storage Url for file
        var imgUrl = (NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)).last! as NSURL
        imgUrl = imgUrl.URLByAppendingPathComponent( "resources/profilepics/\(userId).jpg")
        
        // Check if profile picture already exsists in local device storage
        var errorType : NSError?
        let fileIsAvailable =  imgUrl.checkResourceIsReachableAndReturnError(&errorType)
        
        // Run the file check
        if fileIsAvailable == true {
            // File is available so let's load it from the local storage
            
            userProfilePicture = self.resizeImage(imgUrl)
            
            
        } else {
            // File is not available so let's download it from the corresponding url and store it in the local device storage
            let storageRef = self.storage.referenceForURL("gs://project-8130163388764754339.appspot.com/resources/profilepics")
            let profilePicRef = storageRef.child("\(userId).jpg")
            
            // Begin downloading task
            let downloadTask = profilePicRef.writeToFile(imgUrl) { (URL, error) -> Void in
                if (error != nil) {
                    print(error)
                    userProfilePicture = nil
                } else {
                    // Download was succcessful so let's load the file from the local storage
                    print("success")
                    self.tableView.reloadData()
                    userProfilePicture = self.resizeImage(imgUrl)
                }
            }
        }

        
        
        
        //cell.labelUserId.text = userId
        cell.labelUserName.text = userName
        //cell.labelMatchPercentage.text = String(matchedPercentage) + "%"
        cell.labelMatchPercentage.text = ""
        cell.imageViewProfilePicture.image = userProfilePicture
        
        cell.separatorInset = UIEdgeInsetsZero
        cell.layoutMargins = UIEdgeInsetsZero
        
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        /*
        if segue.identifier == "segueNewConnect" {
            if let indPath = self.tableView.indexPathForSelectedRow {
                let navigationController = segue.destinationViewController as! UINavigationController
                let newConnectVC = navigationController.viewControllers.first as! CreateConnectViewController
                
                print(indPath.row)
                
                let userId = self.matchedUsers[indPath.row].userId
                let userName = self.matchedUsers[indPath.row].userName
                
                newConnectVC.userId = userId
                newConnectVC.userName = userName
                
            }
        }  
        */
        
        if segue.identifier == "segueNewConnect" {
            if let indPath = self.tableView.indexPathForSelectedRow {
                let newConnectVC = segue.destinationViewController as! CreateConnectViewController
                
                let userId = self.matchedUsers[indPath.row].userId
                let userName = self.matchedUsers[indPath.row].userName
                
                newConnectVC.userId = userId
                newConnectVC.userName = userName
                
            }
        }
    }
    
    
    // MARK: - Segues
    
    @IBAction func backToConnectTableViewController(segue: UIStoryboardSegue) {
        
    }
    
    // Mark: -- Actions
    func resizeImage(imgUrl: NSURL) -> UIImage {
        
        let imageSource = CGImageSourceCreateWithURL(imgUrl, nil)
        let options: [NSString: NSObject] = [
            kCGImageSourceThumbnailMaxPixelSize: 300.0,
            kCGImageSourceCreateThumbnailFromImageAlways: true
        ]
        
        return CGImageSourceCreateThumbnailAtIndex(imageSource!, 0, options).flatMap { UIImage(CGImage: $0) }!
        
    }
    
    
}
