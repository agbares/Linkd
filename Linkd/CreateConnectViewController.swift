//
//  CreateConnectViewController.swift
//  Linkd
//
//  Created by Antonio Bares on 6/6/16.
//  Copyright © 2016 jav. All rights reserved.
//

import UIKit
import Firebase
import ImageIO

class CreateConnectViewController: UIViewController, UINavigationControllerDelegate {

    // Matched user
    var userId: String!
    var userName: String!
    
    var userInterestsRef: FIRDatabaseReference!
    var matchedUserInterestsRef: FIRDatabaseReference!
    
    // Outlets
    @IBOutlet var imageViewUserPic: UIImageView!
    @IBOutlet var labelUserName: UILabel!
    @IBOutlet var labelInterests: UILabel!
    @IBOutlet var labelInterest1: UILabel!
    @IBOutlet var labelInterest2: UILabel!
    @IBOutlet var viewInterests: UIView!
    
    // Setup Firebase
    let storage = FIRStorage.storage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Do any additional setup after loading the view.
        self.navigationItem.title = ""
        self.labelUserName.text = self.userName;
        
        self.viewInterests.addTopBorderWithHeight(1.0, andColor: UIColor.grayColor())
        
        // Setup the database references
        self.userInterestsRef = FIRDatabase.database().reference().child(Globals.CONSTANTS.USER_INTERESTS_ROOT)
        self.matchedUserInterestsRef = userInterestsRef.child(self.userId)
        
        // Setup user's profile picture
        
        // Get the local device storage Url for file
        var imgUrl = (NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)).last! as NSURL
        imgUrl = imgUrl.URLByAppendingPathComponent( "resources/profilepics/\(userId).jpg")
        
        // Check if profile picture already exsists in local device storage
        var errorType : NSError?
        let fileIsAvailable =  imgUrl.checkResourceIsReachableAndReturnError(&errorType)
        
        // Run the file check
        if fileIsAvailable == true {
            // File is available so let's load it from the local storage
            
            self.imageViewUserPic.image = self.resizeImage(imgUrl)
            
            
        } else {
            // File is not available so let's download it from the corresponding url and store it in the local device storage
            let storageRef = self.storage.referenceForURL("gs://project-8130163388764754339.appspot.com/resources/profilepics")
            let profilePicRef = storageRef.child("\(self.userId!).jpg")
            
            // Begin downloading task
            let downloadTask = profilePicRef.writeToFile(imgUrl) { (URL, error) -> Void in
                if (error != nil) {
                    print(error)
                    self.imageViewUserPic.image = nil
                } else {
                    // Download was succcessful so let's load the file from the local storage
                    print("success")
                    self.imageViewUserPic.image = self.resizeImage(imgUrl)
                }
            }
        }
        
        
        // Grab the user's interests
        self.matchedUserInterestsRef.observeSingleEventOfType(.Value, withBlock: { (snapshot) -> Void in
            
            let interest1 = snapshot.childSnapshotForPath("interest-1").value?.valueForKey("name") as! String
            let interest2 = snapshot.childSnapshotForPath("interest-2").value?.valueForKey("name") as! String
            
            // We have to put the interests in alphabetical order
            var orderedInterest1: String!
            var orderedInterest2: String!
            
            if interest1 > interest2 {
                orderedInterest1 = interest1
                orderedInterest2 = interest2
            } else {
                orderedInterest1 = interest2
                orderedInterest2 = interest1
            }
            
            self.labelInterest1.text = orderedInterest1
            self.labelInterest2.text = orderedInterest2
            
            
        })
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "segueConnectMessage" {
            let navigationController = segue.destinationViewController as! UINavigationController
            //let newConnectVC = navigationController.viewControllers.first as! CreateConnectViewController
            
            let destinationVC = navigationController.viewControllers.first as! NewConnectMessageViewController
            
            // Get the current user's id
            
            if let user = FIRAuth.auth()?.currentUser {
                let currentUserId = user.uid
                
                
                destinationVC.senderId = currentUserId
                destinationVC.senderName = user.displayName
                
                destinationVC.matchedUserId = self.userId
                destinationVC.matchedUserName = self.userName
            }

        }
        
        
    }
    
    @IBAction func backToCreateConnect(segue: UIStoryboardSegue) {
        
    }
    
    
    
    // Mark: -- Actions
    
    func resizeImage(imgUrl: NSURL) -> UIImage {
        
        let imageSource = CGImageSourceCreateWithURL(imgUrl, nil)
        let options: [NSString: NSObject] = [
            kCGImageSourceThumbnailMaxPixelSize: 500.0,
            kCGImageSourceCreateThumbnailFromImageAlways: true
        ]
        
        return CGImageSourceCreateThumbnailAtIndex(imageSource!, 0, options).flatMap { UIImage(CGImage: $0) }!
        
    }
    
}
