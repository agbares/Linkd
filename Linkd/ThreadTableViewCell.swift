//
//  ThreadTableViewCell.swift
//  Linkd
//
//  Created by Antonio Bares on 5/12/16.
//  Copyright © 2016 jav. All rights reserved.
//

import UIKit

class ThreadTableViewCell: UITableViewCell {
    
    @IBOutlet var labelThreadName: UILabel!
    
    @IBOutlet var labelThreadLatestSender: UILabel!
    
    @IBOutlet var labelThreadLatestMessage: UILabel!
    
    @IBOutlet var imageViewProfilePic: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
