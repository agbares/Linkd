//
//  SettingsTableViewController.swift
//  Linkd
//
//  Created by Antonio Bares on 5/12/16.
//  Copyright © 2016 jav. All rights reserved.
//

import UIKit
import Firebase

class SettingsTableViewController: UITableViewController {

    @IBOutlet var buttonLogOut: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func buttonLogOutAction(sender: AnyObject) {
        try! FIRAuth.auth()!.signOut()
        
        signOut()
    }

    // Make sure when the user clicks a table cell that it registers it to the cell item
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                // Edit Profile
            } else if indexPath.row == 1 {
                self.performSegueWithIdentifier("segueChangePasswordTableViewController", sender: nil)
            }
        }
        
        if indexPath.section == 1 {
            signOut()
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - Actions
    
    // Get the TopViewController
    func topViewController() -> UIViewController {
        var topController = UIApplication.sharedApplication().keyWindow!.rootViewController
        
        while ((topController?.presentedViewController) != nil)  {
            topController = topController?.presentedViewController
        }
        
        return topController!
    }
    
    // This function signs the user out
    func signOut() {
        try! FIRAuth.auth()!.signOut()
        
        let topVC = topViewController()
        let newViewController = self.storyboard?.instantiateViewControllerWithIdentifier("LoginScreen") as! StartScreenViewController
        topVC.loadView()
        print(topVC.viewDidLoad())
        topVC.presentViewController(newViewController, animated: false, completion: nil)
    }
    
    // MARK: - Unwind segues
    
    @IBAction func cancelToSettingsTableViewController(segue: UIStoryboardSegue) {
        
    }
    
    @IBAction func changePassword(segue: UIStoryboardSegue) {
        if let ChangePasswordTableViewController = segue.sourceViewController as? ChangePasswordTableViewController {
            
            let newPassword = ChangePasswordTableViewController.UserNewPassword
            
            let user = FIRAuth.auth()?.currentUser
 
            user?.updatePassword(newPassword) { error in
                if error == nil {
                    print("success")
                } else {
                    print("error")
                }
                
            }
            
        }

    
    }
    
    
    @IBAction func updateInterestsDone(segue: UIStoryboardSegue) {
        
        if let interestsViewController = segue.sourceViewController as? UpdateInterestsTableViewController {
            
            let interest1 = interestsViewController.textFieldInterest1.text!
            let interest2 = interestsViewController.textFieldInterest2.text!
            
            if let user = FIRAuth.auth()?.currentUser {
                
                let userId = user.uid
                
                let userInterestsRef = FIRDatabase.database().reference().child(Globals.CONSTANTS.USER_INTERESTS_ROOT)
                let currentUserInterestsRef = userInterestsRef.child(userId)
                
                currentUserInterestsRef.child("interest-1").setValue(["name": interest1])
                currentUserInterestsRef.child("interest-2").setValue(["name": interest2])

                
            }
            
            
            
        }
        
    }
    
}
