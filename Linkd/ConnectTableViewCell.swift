//
//  ConnectTableViewCell.swift
//  Linkd
//
//  Created by Antonio Bares on 6/6/16.
//  Copyright © 2016 jav. All rights reserved.
//

import UIKit

class ConnectTableViewCell: UITableViewCell {
    
    @IBOutlet var labelUserName: UILabel!
    @IBOutlet var imageViewProfilePicture: UIImageView!
    @IBOutlet var labelMatchPercentage: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
