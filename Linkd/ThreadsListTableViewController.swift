//
//  ThreadsListTableViewController.swift
//  Linkd
//
//  Created by Antonio Bares on 5/12/16.
//  Copyright © 2016 jav. All rights reserved.
//

import UIKit
import Firebase
import CryptoSwift
import ImageIO

class ThreadsListTableViewController: UITableViewController {
    
    @IBOutlet var barButtonItemAdd: UIBarButtonItem!
    
    // Create References for database
    var threadsRef : FIRDatabaseReference!
    var messagesRef: FIRDatabaseReference!
    
    // Create variables for threads
    var threads : [Thread] = []                     // Array to hold all threads
    var threadAlreadyExist = [String : Bool]()      // Dictionary used to check if a thread already exists in the threads array
    var threadPos = [String : Int]()                // Dictionary used to map threads to their positions
    var threadCounter = 0                           // Used to count the number of threads in the array
    
    var userPic : UIImage!
    
    // User credentials
    var senderName = ""
    var senderId = ""
    
    // Thread properties
    var newThreadLatestMessageSender : String!
    var newThreadLatestMessageText : String!
    var newThreadLatestMessageUId : String!
    
    // Setup Firebase
    //var currentThreadRef: FIRDatabaseReference!
    let storage = FIRStorage.storage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        //self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        // Let's get the user's details
        if let user = FIRAuth.auth()?.currentUser {
            
            senderName = user.displayName!
            senderId = user.uid
            
            let userInterestsRef = FIRDatabase.database().reference().child(Globals.CONSTANTS.USER_INTERESTS_ROOT)
            let currentUserInterestsRef = userInterestsRef.child(senderId)
            
            // Let's make sure the user has set their interests
            currentUserInterestsRef.observeSingleEventOfType(.Value, withBlock: { (snapshot) -> Void in
                // If interests have not been set then let's have the user set them up
                if snapshot.exists() == false {
                    print("interests not set!")
                    
                    let destinationVC = self.storyboard?.instantiateViewControllerWithIdentifier("setInterests") as! UINavigationController
                    let updateInterestsVC = destinationVC.viewControllers.first as! UpdateInterestsTableViewController
                    
                    
                    updateInterestsVC.navigationItem.leftBarButtonItem?.enabled = false
                    updateInterestsVC.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.grayColor(), NSFontAttributeName: UIFont(name: "CaviarDreams", size: 20)!], forState: UIControlState.Normal)
                    updateInterestsVC.navigationItem.leftBarButtonItem?.title = ""
                    
                    updateInterestsVC.isFirstRun = true
                    
                    //self.storyboard?.instantiateViewControllerWithIdentifier("connectTable") as! ConnectTableViewController
                    
                    self.presentViewController(destinationVC, animated: true, completion: nil)
                    
                }
            })
        }
        
        self.tabBarController?.tabBar.hidden = false
        
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        
        barButtonItemAdd.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "CaviarDreams-Bold", size: 20)!], forState: UIControlState.Normal)
        
        // Setup the database references

        
    }
    
    override func viewDidAppear(animated: Bool) {

        // Reference for Threads List
        threadsRef = FIRDatabase.database().reference().child(Globals.CONSTANTS.THREADS_ROOT)
        
        // Reference for Threads' messages
        messagesRef = FIRDatabase.database().reference().child(Globals.CONSTANTS.THREADS_MESSAGES_ROOT)
        
        // Look for Threads
        threadsRef.queryOrderedByChild("thread-latest-timestamp").observeEventType(.ChildAdded, withBlock: {(snapshot) in
            let newThreadId = snapshot.value!.valueForKey("thread-id") as! String
            
            // Reference for current thread in messages
            let threadMessageRef = self.messagesRef.child(newThreadId)
            
            let type = snapshot.value!.valueForKey("thread-type") as! String
            
            // If the thread is private then we must check for the current user's subscribed threads list
            // and compare the thread-ids with the current thread-id
            
            // This variable will be used to check if the app should load the thread or not
            var isSubscribed: Bool!
            
            if type == "private" {
                
                // Set up the current user's subscribed threads reference
                let subscribedThreadsRef = FIRDatabase.database().reference().child(Globals.CONSTANTS.USER_SUBSCRIBED_THREADS)
                let currentUserSubscribedThreadsRef = subscribedThreadsRef.child(self.senderId)
                
                currentUserSubscribedThreadsRef.observeSingleEventOfType(.Value, withBlock: { (snapshot) in
                    
                    if snapshot.hasChild(newThreadId) {
                        isSubscribed = true
                    } else {
                        isSubscribed = false
                    }
                    
                    // If the user is subscribed then we can retrieve the thread
                    if isSubscribed == true {
                        
                        // Get latest message in current thread
                        threadMessageRef.queryLimitedToLast(1).observeEventType(.ChildAdded, withBlock: { (snapshot) in
                            
                            let newThreadId = threadMessageRef.key
                            var newThreadName: String!
                            let currentThreadRef = self.threadsRef.child("\(newThreadId)/thread-name")
                            
                            
                            
                            print(snapshot)
                            
                            // Get latest threads
                            currentThreadRef.observeSingleEventOfType(.Value, withBlock: { (snapshot) in
                                newThreadName = snapshot.value! as! String
                                
                                print(snapshot)
                                
                                // Let's get the thread message's info
                                let currentMessageRef = self.messagesRef.child(newThreadId)
                                currentMessageRef.queryLimitedToLast(1).observeSingleEventOfType(.ChildAdded, withBlock: { (snapshot) in
                                    
                                    self.newThreadLatestMessageSender = snapshot.value!.valueForKey("sender-name") as! String
                                    self.newThreadLatestMessageUId = snapshot.value!.valueForKey("sender-id") as! String
                                    
                                    
                                    
                                    if snapshot.value!.valueForKey("isMedia") as! Bool == true {
                                        self.newThreadLatestMessageText = "Attachment: Image"
                                    } else {
                                        self.newThreadLatestMessageText = snapshot.value!.valueForKey("text") as! String
                                    }
                                    
                                    // Check if a thread already exists in the array
                                    if (self.threadAlreadyExist[newThreadId] != nil)
                                    {
                                        // If it already exists then insert it to begining of array
                                        if self.threadAlreadyExist[newThreadId] == true
                                        {
                                            //print("already exists\n")
                                            
                                            // Get position of the old thread
                                            let pos = self.threadPos[newThreadId]!
                                            
                                            // Remove the old thread at that position
                                            self.threads.removeAtIndex(pos)
                                            // Now let's insert the updated thread at the top of the array
                                            self.threads.insert(Thread(name: newThreadName, id: newThreadId, latestMessageSender: self.newThreadLatestMessageSender, latestMessageText: self.newThreadLatestMessageText, latestMessageUId: self.newThreadLatestMessageUId), atIndex: 0)
                                            
                                            
                                            if self.threadPos[newThreadId] != 0 {
                                                // Reassign all threadIds after insertion
                                                for (inThreadId, inPosition) in self.threadPos
                                                {
                                                    if !(inPosition > pos)
                                                    {
                                                        self.threadPos[inThreadId]! = self.threadPos[inThreadId]! + 1
                                                    }
                                                    
                                                }
                                                self.threadPos[newThreadId] = 0
                                            }
                                            
                                            // Reload tableview
                                            self.tableView.beginUpdates()
                                            self.tableView.reloadData()
                                            self.tableView.endUpdates()
                                            
                                        }
                                    }
                                    else
                                    {
                                        // Assign corresponding threadAlreadyExist to true and map the thread's position
                                        self.threadAlreadyExist[newThreadId] = true
                                        
                                        for (inThreadId,  _) in self.threadPos {
                                            self.threadPos[inThreadId]! = self.threadPos[inThreadId]! + 1
                                        }
                                        self.threadCounter += 1
                                        self.threadPos[newThreadId] = 0
                                        
                                        self.threads.insert(Thread(name: newThreadName, id: newThreadId, latestMessageSender: self.newThreadLatestMessageSender, latestMessageText: self.newThreadLatestMessageText, latestMessageUId: self.newThreadLatestMessageUId), atIndex: 0)
                                        
                                        // Update tableview
                                        self.tableView.beginUpdates()
                                        self.tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: 0, inSection: 0)], withRowAnimation: .Automatic)
                                        self.tableView.reloadData()
                                        self.tableView.endUpdates()
                                        
                                    }
                                    
                                }) // End currentMessageRef
                                
                            }) // End currentThreadRef
                            
                        })  // End threadMessageRef
                        
                        
                        
                    }
                    
                })
                
                
                print(newThreadId)
                
            } else {
                
                // Get latest message in current thread
                threadMessageRef.queryLimitedToLast(1).observeEventType(.ChildAdded, withBlock: { (snapshot) in
                    
                    let newThreadId = threadMessageRef.key
                    var newThreadName: String!
                    let currentThreadRef = self.threadsRef.child("\(newThreadId)/thread-name")
                    
                    
                    
                    print(snapshot)
                    
                    // Get latest threads
                    currentThreadRef.observeSingleEventOfType(.Value, withBlock: { (snapshot) in
                        newThreadName = snapshot.value! as! String
                        
                        print(snapshot)
                        
                        // Let's get the thread message's info
                        let currentMessageRef = self.messagesRef.child(newThreadId)
                        currentMessageRef.queryLimitedToLast(1).observeSingleEventOfType(.ChildAdded, withBlock: { (snapshot) in
                            
                            self.newThreadLatestMessageSender = snapshot.value!.valueForKey("sender-name") as! String
                            self.newThreadLatestMessageUId = snapshot.value!.valueForKey("sender-id") as! String
                            
                            
                            
                            if snapshot.value!.valueForKey("isMedia") as! Bool == true {
                                self.newThreadLatestMessageText = "Attachment: Image"
                            } else {
                                self.newThreadLatestMessageText = snapshot.value!.valueForKey("text") as! String
                            }
                            
                            // Check if a thread already exists in the array
                            if (self.threadAlreadyExist[newThreadId] != nil)
                            {
                                // If it already exists then insert it to begining of array
                                if self.threadAlreadyExist[newThreadId] == true
                                {
                                    //print("already exists\n")
                                    
                                    // Get position of the old thread
                                    let pos = self.threadPos[newThreadId]!
                                    
                                    // Remove the old thread at that position
                                    self.threads.removeAtIndex(pos)
                                    // Now let's insert the updated thread at the top of the array
                                    self.threads.insert(Thread(name: newThreadName, id: newThreadId, latestMessageSender: self.newThreadLatestMessageSender, latestMessageText: self.newThreadLatestMessageText, latestMessageUId: self.newThreadLatestMessageUId), atIndex: 0)
                                    
                                    
                                    if self.threadPos[newThreadId] != 0 {
                                        // Reassign all threadIds after insertion
                                        for (inThreadId, inPosition) in self.threadPos
                                        {
                                            if !(inPosition > pos)
                                            {
                                                self.threadPos[inThreadId]! = self.threadPos[inThreadId]! + 1
                                            }
                                            
                                        }
                                        self.threadPos[newThreadId] = 0
                                    }
                                    
                                    // Reload tableview
                                    self.tableView.beginUpdates()
                                    self.tableView.reloadData()
                                    self.tableView.endUpdates()
                                    
                                }
                            }
                            else
                            {
                                // Assign corresponding threadAlreadyExist to true and map the thread's position
                                self.threadAlreadyExist[newThreadId] = true
                                
                                for (inThreadId,  _) in self.threadPos {
                                    self.threadPos[inThreadId]! = self.threadPos[inThreadId]! + 1
                                }
                                self.threadCounter += 1
                                self.threadPos[newThreadId] = 0
                                
                                self.threads.insert(Thread(name: newThreadName, id: newThreadId, latestMessageSender: self.newThreadLatestMessageSender, latestMessageText: self.newThreadLatestMessageText, latestMessageUId: self.newThreadLatestMessageUId), atIndex: 0)
                                
                                // Update tableview
                                self.tableView.beginUpdates()
                                self.tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: 0, inSection: 0)], withRowAnimation: .Automatic)
                                self.tableView.reloadData()
                                self.tableView.endUpdates()
                                
                            }
                            
                        }) // End currentMessageRef
                        
                    }) // End currentThreadRef
                    
                })  // End threadMessageRef
                
                
                
                
            }
            
            
            
        })  // End threadsRef
    }
    
    override func viewDidDisappear(animated: Bool) {
        threads.removeAll()
        threadAlreadyExist.removeAll()
        threadPos.removeAll()
        threadCounter = 0
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return threads.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let reuseIdentifier = "threads"
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier, forIndexPath: indexPath) as! ThreadTableViewCell
        
        // Setup user's profile picture
        
        let UId = self.threads[indexPath.row].latestMessageUId
        
        // Get the local device storage Url for file
        var imgUrl = (NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)).last! as NSURL
        imgUrl = imgUrl.URLByAppendingPathComponent( "resources/profilepics/\(UId!).jpg")
        
        // Check if profile picture already exsists in local device storage
        var errorType : NSError?
        let fileIsAvailable =  imgUrl.checkResourceIsReachableAndReturnError(&errorType)
        
        // Run the file check
        if fileIsAvailable == true {
            // File is available so let's load it from the local storage
            
            self.userPic = self.resizeImage(imgUrl)
            
            
        } else {
            // File is not available so let's download it from the corresponding url and store it in the local device storage
            let storageRef = self.storage.referenceForURL("gs://project-8130163388764754339.appspot.com/resources/profilepics")
            let profilePicRef = storageRef.child("\(UId!).jpg")
            
            // Begin downloading task
            let downloadTask = profilePicRef.writeToFile(imgUrl) { (URL, error) -> Void in
                if (error != nil) {
                    print(error)
                    self.userPic = nil
                } else {
                    // Download was succcessful so let's load the file from the local storage
                    print("success")
                    self.tableView.reloadData()
                    self.userPic = self.resizeImage(imgUrl)
                }
            }
        }
        
        
        // Configure the cell...
        cell.labelThreadName.text = self.threads[indexPath.row].name
        cell.labelThreadLatestSender.text = self.threads[indexPath.row].latestMessageSender
        cell.labelThreadLatestMessage.text = self.threads[indexPath.row].latestMessageText
        cell.imageViewProfilePic.image = userPic
        
        cell.separatorInset = UIEdgeInsetsZero
        cell.layoutMargins = UIEdgeInsetsZero
        
        return cell
    }
    
    // MARK: - dynamic height for tableView
    
    /*
     override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
     return UITableViewAutomaticDimension
     }
     
     override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
     return UITableViewAutomaticDimension
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
     if editingStyle == .Delete {
     // Delete the row from the data source
     tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
     } else if editingStyle == .Insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "showThreadMessages" {
            if let indPath = self.tableView.indexPathForSelectedRow {
                let navigationController = segue.destinationViewController as! UINavigationController
                let threadMessages = navigationController.viewControllers.first as! ThreadMessageViewController
                
                threadMessages.threadId = threads[indPath.row].id
                threadMessages.threadName = threads[indPath.row].name
                threadMessages.senderDisplayName = self.senderName
                threadMessages.senderId = self.senderId
            }
        } else if segue.identifier == "segueNewThread" {
            let newMessagesThread = segue.destinationViewController as! NewMessageNavigationViewController
            newMessagesThread.senderName = self.senderName
            newMessagesThread.senderId = self.senderId
        }
    }
    
    // MARK: - Actions
    
    /* Deprecated -- In favor of navigation bar item in storyboard
     func addTapped() {
     print("tapped\n")
     
     // Segue to create new message
     let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
     
     let vc = mainStoryboard.instantiateViewControllerWithIdentifier("NewMessageViewController") as! NewMessageViewController
     
     vc.senderName = self.senderName
     vc.senderId = self.senderId
     
     self.navigationController?.pushViewController(vc, animated: true)
     }
     */
    
    
    func resizeImage(imgUrl: NSURL) -> UIImage {
        
        let imageSource = CGImageSourceCreateWithURL(imgUrl, nil)
        let options: [NSString: NSObject] = [
            kCGImageSourceThumbnailMaxPixelSize: 300.0,
            kCGImageSourceCreateThumbnailFromImageAlways: true
        ]
        
        return CGImageSourceCreateThumbnailAtIndex(imageSource!, 0, options).flatMap { UIImage(CGImage: $0) }!
        
    }
    
    
    // Mark: - Unwind Segues
    
    @IBAction func backToThreadsListTableViewController(segue: UIStoryboardSegue) {
        
    }
    
    @IBAction func cancelToThreadsListTableViewController(segue: UIStoryboardSegue) {
        
    }
    
    @IBAction func addNewThread(segue: UIStoryboardSegue) {
        if let NewMessageTableViewController = segue.sourceViewController as? NewMessageTableViewController {
            
            let newThreadName = NewMessageTableViewController.newThreadName
            let newThreadMessage = NewMessageTableViewController.newThreadMessage
            
            
            // Thread Id Hash
            let dateStr = String(NSDate().timeIntervalSince1970)
            let data : NSData = dateStr.dataUsingEncoding(NSUTF8StringEncoding)!
            let hash = String(data.md5())
            let newThreadId = hash
            
            // Check user input
            
            if newThreadName!.characters.count < 1 || newThreadMessage!.characters.count < 1 {
                
                // Show error to user
                self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(red: 0.8196, green: 0.2549, blue: 0.1294, alpha: 1.0)]
                self.navigationItem.title = "All fields are required"
                
                // Delay for 5 seconds before reverting back to original navigation bar
                let triggerTime = (Int64(NSEC_PER_SEC) * 5)
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, triggerTime), dispatch_get_main_queue(), { () -> Void in
                    
                    self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
                    self.navigationItem.title = "THREADS"
                })
                
                
            } else {
                
                
                // Get key for new thread
                let key = threadsRef.childByAutoId().key
                
                let newTimestampAsString = Int(NSDate().timeIntervalSince1970)
                let newThread = ["thread-creator-id": senderId, "thread-id" : "\(key)", "thread-name" : "\(newThreadName!)", "thread-latest-timestamp": newTimestampAsString, "thread-type": "public"]
                
                let newMessage = [
                    "sender-name": senderName,
                    "sender-id": senderId,
                    "text": newThreadMessage!,
                    "hash": UInt(NSDate().timeIntervalSince1970 * 100000),
                    "isMedia": false,
                    "mediaUrl": ""
                    
                ]
                
                threadsRef.child(key).setValue(newThread)
                messagesRef.child("\(key)").childByAutoId().setValue(newMessage)
            }
            
            
        }
    }
    
    @IBAction func createNewConnect(segue: UIStoryboardSegue) {
        
        
        let sourceVC = segue.sourceViewController as! NewConnectMessageViewController
        self.senderId = sourceVC.senderId
        self.senderName = sourceVC.senderName
        
        // Make sure we setup the threads and messages reference
        threadsRef = FIRDatabase.database().reference().child(Globals.CONSTANTS.THREADS_ROOT)
        messagesRef = FIRDatabase.database().reference().child(Globals.CONSTANTS.THREADS_MESSAGES_ROOT)
        let connectionsRef = FIRDatabase.database().reference().child(Globals.CONSTANTS.USER_CONNECTIONS_ROOT)
        
        let newThreadName = sourceVC.textFieldThreadName.text!
        let newThreadMessage = sourceVC.textFieldMessage.text!
        
        print(newThreadName)
        print(newThreadMessage)
        
        let matchedUserId = sourceVC.matchedUserId
        
        // Setup the subscribed threads references
        let subscribedThreadsRef = FIRDatabase.database().reference().child(Globals.CONSTANTS.USER_SUBSCRIBED_THREADS)
        let currentUserSubscribedThreadsRef = subscribedThreadsRef.child(self.senderId)
        let matchedUserSubscribedThreadsRef = subscribedThreadsRef.child(matchedUserId)
        
        // Setup the connections reference
        let currentConnectionsRef = connectionsRef.child(self.senderId)
        let matchedUserConnectionsRef = connectionsRef.child(matchedUserId)
        
        // Make sure we set their connections to prevent them from finding eachother on connections again
        currentConnectionsRef.child(matchedUserId).setValue(["isConnected": true])
        matchedUserConnectionsRef.child(self.senderId).setValue(["isConnected": true])
        
        // Get key for new thread
        let key = threadsRef.childByAutoId().key
        
        print(key)
        
        // Set the subscriptions for the users
        // NOTE: isSubscribed is just arbriturary. It is only needed to
        // Add the new thread id to the subscription list
        currentUserSubscribedThreadsRef.child(key).setValue(["isSubscribed": true])
        matchedUserSubscribedThreadsRef.child(key).setValue(["isSubscribed": true])
        
        // Thread Id Hash
        let dateStr = String(NSDate().timeIntervalSince1970)
        let data : NSData = dateStr.dataUsingEncoding(NSUTF8StringEncoding)!
        let hash = String(data.md5())
        let newThreadId = hash
        
        let newTimestampAsString = Int(NSDate().timeIntervalSince1970)
        let newThread = ["thread-creator-id": senderId, "thread-id" : "\(key)", "thread-name" : "\(newThreadName)", "thread-latest-timestamp": newTimestampAsString, "thread-type": "private"]
        
        let newMessage = [
            "sender-name": senderName,
            "sender-id": senderId,
            "text": newThreadMessage,
            "hash": UInt(NSDate().timeIntervalSince1970 * 100000),
            "isMedia": false,
            "mediaUrl": ""
            
        ]
        
        threadsRef.child(key).setValue(newThread)
        messagesRef.child("\(key)").childByAutoId().setValue(newMessage)
        
    }
    
}


