//
//  MatchedUser.swift
//  Linkd
//
//  Created by Antonio Bares on 6/6/16.
//  Copyright © 2016 jav. All rights reserved.
//

import UIKit

class MatchedUser: NSObject {

    var userId: String
    var userName: String
    var matchedPercentage: Float
 
    init(userId: String, userName: String, matchedPercentage: Float) {
        self.userId = userId
        self.userName = userName
        self.matchedPercentage = matchedPercentage
    }
    
}
