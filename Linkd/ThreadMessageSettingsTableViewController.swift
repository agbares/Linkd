//
//  ThreadMessageSettingsTableViewController.swift
//  Linkd
//
//  Created by Antonio Bares on 5/24/16.
//  Copyright © 2016 jav. All rights reserved.
//

import UIKit

class ThreadMessageSettingsTableViewController: UITableViewController, UITextFieldDelegate {
    
    @IBOutlet var textFieldThreadName: UITextField!
    @IBOutlet var buttonChangeThreadName: UIButton!
    
    var threadName: String!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        textFieldThreadName.placeholder = threadName
        //buttonChangeThreadName.tintColor = UIColor.grayColor()
        
        buttonChangeThreadName.setTitleColor(UIColor.grayColor(), forState: .Normal)
        
        textFieldThreadName.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func textFieldThreadNameEditBegin(sender: AnyObject) {
        buttonChangeThreadName.tintColor = UIColor.grayColor()
        buttonChangeThreadName.enabled = false
    }
    
    @IBAction func textFieldThreadNameEditEnd(sender: AnyObject) {
        if textFieldThreadName.text?.characters.count > 0 {
            buttonChangeThreadName.enabled = true
            //buttonChangeThreadName.tintColor = UIColor(red: 0.8, green: 0.2431, blue: 0, alpha: 1.0)
            buttonChangeThreadName.setTitleColor(UIColor(red: 0.8, green: 0.2431, blue: 0, alpha: 1.0), forState: .Normal)
            //buttonChangeThreadName.color
        } else {
            buttonChangeThreadName.setTitleColor(UIColor.grayColor(), forState: .Normal)
            buttonChangeThreadName.enabled = false
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == textFieldThreadName {
            self.dismissKeyboard()
        }
        return true
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
