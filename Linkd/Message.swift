//
//  Message.swift
//  Linkd
//
//  Created by Antonio Bares on 5/21/16.
//  Copyright © 2016 jav. All rights reserved.
//

import Foundation
import JSQMessagesViewController

class Message: NSObject, JSQMessageData {
    var msgSenderName: String
    var msgSenderId: String
    var msgText: String
    var msgHash: UInt
    var msgDate: NSDate
    var msgMedia: JSQMediaItem
    var isMedia: Bool
    
    var msgMediaNil: JSQPhotoMediaItem = JSQPhotoMediaItem.init()
    
    /*
     convenience init(senderName: String, senderId: String, text: String, hash: UInt) {
     self.init(senderName: senderName, senderId: senderId, text: text, msgMedia: self.msgMediaNil, hash: hash, imageUrl: nil)
     }
     */
    
    
    init(senderName: String, senderId: String, text: String, hash: UInt, isMedia: Bool, media: JSQMediaItem)
    {
        self.msgSenderName = senderName
        self.msgSenderId = senderId
        self.msgText = text
        self.msgHash = hash
        self.msgDate = NSDate()
        self.isMedia = isMedia
        
        if self.isMedia == true {
            self.msgMedia = media
            
        } else {
            self.msgMedia = msgMediaNil
        }
        
    }
    
    func senderId() -> String! {
        return msgSenderId
    }
    
    func senderDisplayName() -> String! {
        return msgSenderName
    }
    
    func date() -> NSDate! {
        return msgDate
    }
    
    func isMediaMessage() -> Bool {
        if isMedia == true {
            return true
        }
        return false
    }
    
    func media() -> JSQMessageMediaData! {
        if isMedia == true {
            return msgMedia
        }
        return msgMediaNil
    }
    
    func messageHash() -> UInt {
        return msgHash
    }
    
    func text() -> String! {
        if isMedia == true {
            return nil
        }
        return msgText
    }
    
}
