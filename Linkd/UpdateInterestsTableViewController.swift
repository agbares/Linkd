//
//  UpdateInterestsTableViewController.swift
//  Linkd
//
//  Created by Antonio Bares on 5/25/16.
//  Copyright © 2016 jav. All rights reserved.
//


import UIKit
import Firebase

class UpdateInterestsTableViewController: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet var textFieldInterest1: UITextField!
    @IBOutlet var textFieldInterest2: UITextField!
    
    @IBOutlet var barButtonItemDone: UIBarButtonItem!
    
    let pickerView1 = UIPickerView()
    let pickerView2 = UIPickerView()
    
    // Used to validate user interest choices
    var isPickedValue1Valid = false
    var isPickedValue2Valid = false
    
    var pickerView1DoneButton: UIBarButtonItem!
    var pickerView2DoneButton: UIBarButtonItem!
    
    var pickerData: [String] = [String]()

    var isFirstRun: Bool! = false
    
    // Navigation bar items
    //let navBarRightItem = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: #selector())
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        barButtonItemDone.enabled = false
        barButtonItemDone.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.grayColor(), NSFontAttributeName: UIFont(name: "CaviarDreams", size: 20)!], forState: .Normal)
        

        // pickerView 1
        pickerView1.delegate = self
        pickerView1.dataSource = self
        pickerView1.showsSelectionIndicator = true
        
        // PickerView 1 toolbar
        let pickerView1ToolBar = UIToolbar()
        pickerView1ToolBar.barStyle = .Default
        pickerView1ToolBar.translucent = true
        pickerView1ToolBar.sizeToFit()
        
        pickerView1DoneButton = UIBarButtonItem(title: "Done", style: .Plain, target: self, action: #selector(donePicker))
        pickerView1DoneButton.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.blackColor()], forState: .Normal)
        pickerView1ToolBar.setItems([pickerView1DoneButton], animated: false)
        pickerView1ToolBar.userInteractionEnabled = true
        
        textFieldInterest1.inputView = pickerView1
        textFieldInterest1.inputAccessoryView = pickerView1ToolBar
        
        // pickerView 2
        pickerView2.delegate = self
        pickerView2.dataSource = self
        pickerView2.showsSelectionIndicator = true

        // pickerView 2 toolbar
        let pickerView2ToolBar = UIToolbar()
        pickerView2ToolBar.barStyle = .Default
        pickerView2ToolBar.translucent = true
        pickerView2ToolBar.sizeToFit()
        
        pickerView2DoneButton = UIBarButtonItem(title: "Done", style: .Plain, target: self, action: #selector(donePicker))
        pickerView2DoneButton.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.blackColor()], forState: .Normal)
        pickerView2ToolBar.setItems([pickerView2DoneButton], animated: false)
        pickerView2ToolBar.userInteractionEnabled = true
        
        textFieldInterest2.inputView = pickerView2
        textFieldInterest2.inputAccessoryView = pickerView2ToolBar
        
        //addNewInterests()
        getInterests()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Setup pickerView
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerView1 {
            print(pickerData[row])
        } else if pickerView == pickerView2 {
            print(pickerData[row])
        }

    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 1 {
            textFieldInterest1.becomeFirstResponder()
        } else if indexPath.row == 2 {
            textFieldInterest2.becomeFirstResponder()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK: - Actions
    
    func donePicker(barButtonItem: UIBarButtonItem) {
        
        if barButtonItem == pickerView1DoneButton {
            textFieldInterest1.text = pickerData[pickerView1.selectedRowInComponent(0)]
            if pickerView1.selectedRowInComponent(0) != 0 {
                isPickedValue1Valid = true
                
            }
        } else if barButtonItem == pickerView2DoneButton {
            textFieldInterest2.text = pickerData[pickerView2.selectedRowInComponent(0)]
            if pickerView2.selectedRowInComponent(0) != 0 {
                isPickedValue2Valid = true
            }
        }
        
        if isPickedValue1Valid == true && isPickedValue2Valid == true && textFieldInterest1.text != textFieldInterest2.text {
            barButtonItemDone.enabled = true
                    barButtonItemDone.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "CaviarDreams", size: 20)!], forState: .Normal)
        }
        
        print(barButtonItem)
        
        dismissKeyboard()
    }
    
    // Let's make sure we disable the done button when the user opens the UIPickerView
    @IBAction func textFieldInterest1EditBegin(sender: AnyObject) {
        barButtonItemDone.enabled = false
        barButtonItemDone.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.grayColor(), NSFontAttributeName: UIFont(name: "CaviarDreams", size: 20)!], forState: .Normal)
        isPickedValue1Valid = false
    }
    
    @IBAction func textFieldInterest2EditDidBegin(sender: AnyObject) {
        barButtonItemDone.enabled = false
        barButtonItemDone.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.grayColor(), NSFontAttributeName: UIFont(name: "CaviarDreams", size: 20)!], forState: .Normal)
        isPickedValue2Valid = false
    }
    
    
    /*
        getInterests()
        This function retrieves all the interests from the Firebase database and appends it to the pickerData array
 
    */
    func getInterests() {
        
        let interestsRef = FIRDatabase.database().reference().child(Globals.CONSTANTS.INTERESTS_ROOT)
        
        interestsRef.queryOrderedByChild("name").observeEventType(.ChildAdded, withBlock: { snapshot in
            //print(snapshot)
        
            //print(snapshot.value!.valueForKey("name") as! String)
            
            let pickerVal = snapshot.value!.valueForKey("name") as! String
            
            self.pickerData.append(pickerVal)
            self.pickerView1.reloadAllComponents()
            self.pickerView2.reloadAllComponents()
            
        })
    }
    
    /*
        addNewInterests()
        This function allows you to add new interests to the database.
        NOTE: Make sure these get called only once. Otherwise the database will have duplicates of the following interests.
    */
    func addNewInterests() {
        let interestsRef = FIRDatabase.database().reference().child(Globals.CONSTANTS.INTERESTS_ROOT)
        
        let interestsMatchPercentagesRef = FIRDatabase.database().reference().child(Globals.CONSTANTS.INTERESTS_MATCH_PERCENTAGES_ROOT)
        
        interestsRef.childByAutoId().setValue(["name": "Sports"])
        var currentInterestRef = interestsMatchPercentagesRef.child("Sports")
        currentInterestRef.setValue(["Sports": 1.0, "Technology": 0.4, "Literature": 0.1, "Music": 0.05, "Art": 0.25, "Food": 0.05])
        
        interestsRef.childByAutoId().setValue(["name": "Technology"])
        currentInterestRef = interestsMatchPercentagesRef.child("Technology")
        currentInterestRef.setValue(["Sports": 0.4, "Technology": 1.0, "Literature": 0.2, "Music": 0.25, "Art": 0.4, "Food": 0.2])
        
        interestsRef.childByAutoId().setValue(["name": "Literature"])
        currentInterestRef = interestsMatchPercentagesRef.child("Literature")
        currentInterestRef.setValue(["Sports": 0.1, "Technology": 0.2, "Literature": 1.0, "Music": 0.4, "Art": 0.75, "Food": 0.75])
        
        interestsRef.childByAutoId().setValue(["name": "Music"])
        currentInterestRef = interestsMatchPercentagesRef.child("Music")
        currentInterestRef.setValue(["Sports": 0.05, "Technology": 0.25, "Literature": 0.4, "Music": 1.0, "Art": 0.8, "Food": 0.1])
        
        interestsRef.childByAutoId().setValue(["name": "Art"])
        currentInterestRef = interestsMatchPercentagesRef.child("Art")
        currentInterestRef.setValue(["Sports": 0.25, "Technology": 0.4, "Literature": 0.75, "Music": 0.8, "Art": 1.0, "Food": 0.3])
        
        interestsRef.childByAutoId().setValue(["name": "Food"])
        currentInterestRef = interestsMatchPercentagesRef.child("Food")
        currentInterestRef.setValue(["Sports": 0.4, "Technology": 0.2, "Literature": 0.75, "Music": 0.25, "Art": 0.4, "Food": 1.0])
        
        interestsRef.childByAutoId().setValue(["name": ""])
    }
    
    
}
