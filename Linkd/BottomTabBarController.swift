//
//  BottomTabBarController.swift
//  Linkd
//
//  Created by Antonio Bares on 5/21/16.
//  Copyright © 2016 jav. All rights reserved.
//

import UIKit
import Firebase

class BottomTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setTabBarColor()
        
        
        // Let's check if the user is logged in. If not then send them back to the login screen
        /*
        FIRAuth.auth()!.addAuthStateDidChangeListener { auth, user in
            if let user = user {
                // User logged in
                print(user.displayName)
            } else {
                print("not logged in")
                let newViewController = self.storyboard?.instantiateViewControllerWithIdentifier("LoginScreen") as! StartScreenViewController
                newViewController.loadView()
                newViewController.viewLogIn.hidden = false
                newViewController.presentLoginView()
                newViewController.labelSignInAlert.text = "Not signed in"
                newViewController.labelSignInAlert.hidden = false
                self.presentViewController(newViewController, animated: false, completion: nil)
            }
        }
 */
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setTabBarColor() {
        let unselectedColor = UIColor(red: 0.8784, green: 0.8784, blue: 0.8784, alpha: 1.0)
        for item in self.tabBar.items! {
            item.image = item.selectedImage?.imageWithColor(unselectedColor).imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
            //In case you wish to change the font color as well
            let attributes = [NSForegroundColorAttributeName: unselectedColor]
            item.setTitleTextAttributes(attributes, forState: UIControlState.Normal)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIImage {
    func imageWithColor(color1: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color1.setFill()
        
        let context = UIGraphicsGetCurrentContext()
        CGContextTranslateCTM(context, 0, self.size.height)
        CGContextScaleCTM(context, 1.0, -1.0);
        CGContextSetBlendMode(context, CGBlendMode.Normal)
        
        let rect = CGRectMake(0, 0, self.size.width, self.size.height) as CGRect
        CGContextClipToMask(context, rect, self.CGImage)
        CGContextFillRect(context, rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext() as UIImage
        UIGraphicsEndImageContext()
        
        return newImage
    }
}