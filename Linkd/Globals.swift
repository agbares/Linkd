//
//  Globals.swift
//  Linkd
//
//  Created by Antonio Bares on 5/26/16.
//  Copyright © 2016 jav. All rights reserved.
//

import UIKit

class Globals: NSObject {
    
    // Constants for application
    struct CONSTANTS {
        
        // User data
        static let USER_DATA_ROOT                       =   "user-data"
        static let USER_BASIC_INFO_ROOT                 =   "\(USER_DATA_ROOT)/basic-info"
        static let GROUPS_ROOT                          =   "\(USER_DATA_ROOT)/groups"
        static let USER_INTERESTS_ROOT                  =   "\(USER_DATA_ROOT)/interests"
        static let USER_SUBSCRIBED_THREADS              =   "\(USER_DATA_ROOT)/subscribed-threads"
        static let USER_CONNECTIONS_ROOT                =   "\(USER_DATA_ROOT)/connections"
        
        
        // Messaging system
        static let MESSAGING_SYSTEM_ROOT                =   "messaging-system"
        static let THREADS_ROOT                         =   "\(MESSAGING_SYSTEM_ROOT)/threads"
        static let THREADS_MESSAGES_ROOT                =   "\(MESSAGING_SYSTEM_ROOT)/messages"
        
        // Matching system
        static let MATCHING_SYSTEM_ROOT                 =   "matching-system"
        static let INTERESTS_ROOT                       =   "\(MATCHING_SYSTEM_ROOT)/interests"
        static let INTERESTS_MATCH_PERCENTAGES_ROOT     =   "\(MATCHING_SYSTEM_ROOT)/interests-match-perecentages"
    }

    
    
}
