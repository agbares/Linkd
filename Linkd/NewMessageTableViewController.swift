//
//  NewMessageTableViewController.swift
//  Linkd
//
//  Created by Antonio Bares on 5/21/16.
//  Copyright © 2016 jav. All rights reserved.
//

import UIKit
import Firebase

class NewMessageTableViewController: UITableViewController, UITextFieldDelegate {
    
    // Outlets
    @IBOutlet var textFieldThreadName: UITextField!
    @IBOutlet var textFieldMessage: UITextField!
    @IBOutlet var buttonDone: UIBarButtonItem!
    
    var senderName : String?
    var senderId : String?
    
    //var newThread = [String : String]()
    //var newThreadMessage = [String : String]()
    
    var newThreadName : String?
    var newThreadMessage: String?
    
    var dateStr : String?
    var data : NSData?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        // Assign delegates for return keys
        self.textFieldThreadName.delegate = self
        self.textFieldMessage.delegate = self

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Make sure when the user clicks a table cell that it goes to the textField
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0 {
            textFieldThreadName.becomeFirstResponder()
        } else {
            textFieldMessage.becomeFirstResponder()
        }
    }
    
    // Catch return keys
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == textFieldThreadName {
            textFieldMessage.becomeFirstResponder()
            
        } else if textField == textFieldMessage {
            textFieldMessage.resignFirstResponder()
        }
        
        return true
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "segueAddNewThread" {
            self.newThreadName = textFieldThreadName.text
            self.newThreadMessage = textFieldMessage.text
        }
    }
    

}
