//
//  Thread.swift
//  Linkd
//
//  Created by Antonio Bares on 5/21/16.
//  Copyright © 2016 jav. All rights reserved.
//

import UIKit

class Thread: NSObject {
    
    var name : String?
    var id : String?
    var latestMessageSender: String?
    var latestMessageText : String?
    var latestMessageUId : String?
    
    init(name: String, id: String, latestMessageSender: String, latestMessageText: String, latestMessageUId : String) {
        self.name = name
        self.id = id
        self.latestMessageSender = latestMessageSender
        self.latestMessageText = latestMessageText
        self.latestMessageUId = latestMessageUId
    }
    
    
}
