//
//  StartScreenViewController.swift
//  Linkd
//
//  Created by Antonio Bares on 5/12/16.
//  Copyright © 2016 jav. All rights reserved.
//

import UIKit
import Firebase
import ALCameraViewController
import ImageIO

class StartScreenViewController: UIViewController, UITextFieldDelegate {
    
    // Variable for photoUrl during Sign Up Photo View
    var newUrl : String!
    
    // Holds error codes
    var errorCode : Int!
    
    // Maps error codes
    var errorCodeMap = [Int : String]()
    
    // UIViews for Signing in and Signing Up
    @IBOutlet var viewLogIn: UIView!
    @IBOutlet var viewSignUpPage1: UIView!
    @IBOutlet var viewSignUpPage2: UIView!
    @IBOutlet var viewUploadPhoto: UIView!
    
    @IBOutlet var imageViewBackdrop: UIImageView!
    
    @IBOutlet var buttonConnect: UIButton!
    
    /** MARK: - Login View Outlets **/
    
    // Detail Labels
    @IBOutlet var labelLoginEmail: UILabel!
    @IBOutlet var labelLoginPassword: UILabel!
    
    // Textfields
    @IBOutlet var textfieldLoginEmail: UITextField!
    @IBOutlet var textfieldLoginPassword: UITextField!
    
    // Alert
    @IBOutlet var labelSignInAlert: UILabel!
    
    /** Mark: - Sign Up View Page 1 Outlets **/
    
    // Detail Labels
    @IBOutlet var labelSignUpFirstName: UILabel!
    @IBOutlet var labelSignUpLastName: UILabel!
    @IBOutlet var labelSignUpEmail: UILabel!
    
    // Textfields
    @IBOutlet var textFieldSignUpFirstName: UITextField!
    @IBOutlet var textFieldSignUpLastName: UITextField!
    @IBOutlet var textFieldSignUpEmail: UITextField!
    
    // Alert
    @IBOutlet var labelSignUpAlertPage1: UILabel!
    
    
    /** MARK: - Sign Up View Page 2 Outlets **/
    
    // Detail Labels
    @IBOutlet var labelSignUpPassword: UILabel!
    @IBOutlet var labelSignUpPasswordConfirm: UILabel!
    
    // Textfields
    @IBOutlet var textFieldSignUpPassword: UITextField!
    @IBOutlet var textFieldSignUpPasswordConfirm: UITextField!

    // Alert
    @IBOutlet var labelSignUpAlertPage2: UILabel!
    
    
    /** MARK: - Upload Photo View Outlets **/
    
    @IBOutlet var buttonUploadPhoto: UIButton!
    
    // Alert
    @IBOutlet var labelUploadPhotoAlert: UILabel!
    

    
    
    /** Login View Actions **/
    
    // If user begins typing, hide email label
    @IBAction func textfieldLoginEmailEditBegin(sender: AnyObject) {
        labelLoginEmail.hidden = true
    }
    // When user exits out of textfield check for text, if empty unhide email label
    @IBAction func textfieldLoginEditEnd(sender: AnyObject) {
        if textfieldLoginEmail.text?.characters.count < 1
        {
            labelLoginEmail.hidden = false
        }
    }
    
    // If user begins typing, hide email label
    @IBAction func textfieldPasswordEditBegin(sender: AnyObject) {
        labelLoginPassword.hidden = true
    }
    
    // When user exits out of textfield check for text, if empty unhide email label
    @IBAction func textfieldPasswordEditEnd(sender: AnyObject) {
        if textfieldLoginPassword.text?.characters.count < 1
        {
            labelLoginPassword.hidden = false
        }
    }
    
    // Login button action
    @IBAction func buttonLogin(sender: AnyObject) {
        labelSignInAlert.hidden = true
        
        // Let's validate the textFields first
        if self.textfieldLoginEmail.text!.characters.count < 1 || self.textfieldLoginPassword.text!.characters.count < 1 {
            labelSignInAlert.text = "Invalid credentials"
            labelSignInAlert.hidden = false
        } else {
            // User actually inputed text into the fields so let's validate the login
            FIRAuth.auth()!.signInWithEmail(String(self.textfieldLoginEmail.text!), password: String(self.textfieldLoginPassword.text!)) { (user, error) in
                if let error = error {
                    print(error.code)
                    
                    if error.code == 17009 {
                        self.labelSignInAlert.text = "Invalid password"
                        self.labelSignInAlert.hidden = false
                        
                    } else if error.code == 17011 {
                        self.labelSignInAlert.text = "Invalid credentials"
                        self.labelSignInAlert.hidden = false
                        
                    } else if error.code == 17999 {
                        self.labelSignInAlert.text = "Invalid email"
                        self.labelSignInAlert.hidden = false
                        
                    }
                    
                } else {
                    self.performSegueWithIdentifier("segueLoginSuccess", sender: self)
                    
                }
            }
        }
    }
    
    // Sign Up button action
    @IBAction func buttonSignUp(sender: AnyObject) {
        self.labelSignInAlert.hidden = true
        presentViewSignUpPage1()
    }
    
    /** Sign Up View Page 1 Actions **/
    
    // Hide detail labels
    @IBAction func textFieldSignUpEmailEditBegin(sender: AnyObject) {
        labelSignUpEmail.hidden = true
    }
    
    @IBAction func textFieldSignUpPasswordEditBegin(sender: AnyObject) {
        labelSignUpPassword.hidden = true
    }
    
    @IBAction func textFieldSignUpPasswordConfirmEditBegin(sender: AnyObject) {
        labelSignUpPasswordConfirm.hidden = true
    }
    
    // Show detail labels
    @IBAction func textFieldSignUpEmailEditEnd(sender: AnyObject) {
        if textFieldSignUpEmail.text?.characters.count < 1
        {
            labelSignUpEmail.hidden = false
        }
    }
    
    @IBAction func textFieldSignUpPasswordEditEnd(sender: AnyObject) {
        if textFieldSignUpPassword.text?.characters.count < 1
        {
            labelSignUpPassword.hidden = false
        }
    }
    
    @IBAction func textFieldSignUpPasswordConfirmEditEnd(sender: AnyObject) {
        if textFieldSignUpPasswordConfirm.text?.characters.count < 1
        {
            labelSignUpPasswordConfirm.hidden = false
        }
    }
    
    // Cancel button action - go back to sign in page
    @IBAction func buttonSignUpPage1Cancel(sender: AnyObject) {
        viewSignUpPage1.hidden = true
        viewLogIn.hidden = false
        self.labelLoginEmail.hidden = false
        
        // If user had previously typed email in the sign in let's keep it there
        if self.textfieldLoginEmail.text?.characters.count < 1 {
            self.labelLoginEmail.hidden = false
        } else if self.textfieldLoginEmail.text?.characters.count == 1 &&
       self.textfieldLoginEmail.text?.characters.first == " " {
            self.textfieldLoginEmail.text = ""
            self.labelLoginEmail.hidden = false
        } else {
            self.labelLoginEmail.hidden = true
        }
        
        // Take out any previously typed in password in the sign in page
        self.textfieldLoginPassword.text = ""
        self.labelLoginPassword.hidden = false
        
        self.dismissKeyboard()
    }
    
    // Confirm button action - Bring up next page
    @IBAction func buttonSignUpPage1Confirm(sender: AnyObject) {
        labelSignUpAlertPage1.hidden = true
        
        // Validate textfield input
        if textFieldSignUpEmail.text?.characters.count < 1 || textFieldSignUpPassword.text?.characters.count < 1 || textFieldSignUpPasswordConfirm.text?.characters.count < 1 {
            labelSignUpAlertPage1.text = "All fields are required"
            labelSignUpAlertPage1.hidden = false
            
        } else if textFieldSignUpPassword.text?.characters.count < 7 {
            labelSignUpAlertPage1.text = "Password must be 8 or more characters"
            labelSignUpAlertPage1.hidden = false
            
        } else if textFieldSignUpPassword.text != textFieldSignUpPasswordConfirm.text {
            labelSignUpAlertPage1.text = "Passwords do not match"
            labelSignUpAlertPage1.hidden = false

        } else {
            // Let's sign the user up
            FIRAuth.auth()?.createUserWithEmail(textFieldSignUpEmail.text!, password: textFieldSignUpPassword.text!) { (user, error) in
                
                self.errorCode = nil
                
                // If no error let's go ahead present the next signup page
                if(error == nil) {
                    // Success!
                    self.labelSignUpAlertPage1.hidden = true
                    self.presentViewSignUpPage2()
                    
                } else {
                    // Error creating account so let's display the error to the user
                    print(error!.code)
                    print(error?.localizedDescription)
                    print(error?.localizedRecoverySuggestion)
                    print(error?.localizedFailureReason)
                    print(error?.localizedRecoveryOptions)
                    
                    self.errorCode = error!.code
                    
                    self.labelSignUpAlertPage1.text = self.errorCodeMap[self.errorCode]
                    self.labelSignUpAlertPage1.hidden = false
                }
                
            }
        }
        
    }
    
    
    /** Sign Up View Page 2 Actions **/
    
    // Hide detail labels
    @IBAction func textFieldSignUpFirstNameEditBegin(sender: AnyObject) {
        labelSignUpFirstName.hidden = true
    }
    
    @IBAction func textFieldSignUpLastNameEditBegin(sender: AnyObject) {
        labelSignUpLastName.hidden = true
    }
    

    
    // Show detail labels
    @IBAction func textFieldSignUpFirstNameEditEnd(sender: AnyObject) {
        if textFieldSignUpFirstName.text?.characters.count < 1
        {
            labelSignUpFirstName.hidden = false
        }
    }
    
    @IBAction func textFieldSignUpLastNameEditEnd(sender: AnyObject) {
        if textFieldSignUpLastName.text?.characters.count < 1
        {
            labelSignUpLastName.hidden = false
        }
    }
    
    // Back button action - Go back to Sign up view page 1
    @IBAction func buttonSignUpPage2Back(sender: AnyObject) {
        // Since the user already made the account through Firebase we have to first delete the account
        let user = FIRAuth.auth()?.currentUser
        
        user?.deleteWithCompletion { error in
            if error == nil {
                // User deletion successful so let's go back to previous page
                self.textFieldSignUpPassword.text = ""
                self.textFieldSignUpPasswordConfirm.text = ""
                self.labelSignUpPassword.hidden = false
                self.labelSignUpPasswordConfirm.hidden = false
                self.viewSignUpPage2.hidden = true
                self.viewSignUpPage1.hidden = false
                self.dismissKeyboard()
            } else {
                // An error ocurred
                print("Error in deleting")
            }
        }
    }
    
    
    // Confirm button action - Bring up photo view
    @IBAction func buttonSignUpPage2Confirm(sender: AnyObject) {
        labelSignUpAlertPage2.hidden = true
        
        // Validate textfield input
        if textFieldSignUpFirstName.text?.characters.count < 1 || textFieldSignUpLastName.text?.characters.count < 1 {
            labelSignUpAlertPage2.text = "All fields are required"
            labelSignUpAlertPage2.hidden = false
        } else {
            
            // Let's change the user's display name to the entered name & update the user's basic info
            let user = FIRAuth.auth()?.currentUser
            if let user = user {
                let changeRequest = user.profileChangeRequest()
                
                // Strip off any trailing whitespaces from the name
                let firstName = self.stripTrailingWhitespace(textFieldSignUpFirstName.text!)
                let lastName = self.stripTrailingWhitespace(textFieldSignUpLastName.text!)
                
                changeRequest.displayName = "\(firstName) \(lastName)"
                
                // Let's push the update
                changeRequest.commitChangesWithCompletion { error in
                    
                    // If no error, then let's go ahead and present the photo upload view
                    if error == nil {
                        self.labelSignUpAlertPage2.hidden = true
                        self.presentViewUploadPhoto()
                    } else {
                        // Display the error to the user
                        self.labelSignUpAlertPage2.text = error?.localizedDescription
                        self.labelSignUpAlertPage1.hidden = false
                    }
                }
                
                // Let's set the user's basic info
                let basicUserInfoRef = FIRDatabase.database().reference().child(Globals.CONSTANTS.USER_BASIC_INFO_ROOT)
                let currentBasicUserInfoRef = basicUserInfoRef.child(user.uid)
                currentBasicUserInfoRef.setValue(["Name": "\(firstName) \(lastName)", "Email": user.email!])
            }
        }
    }
    
    /** Upload photo view **/
    
    
    // Select photo action
    @IBAction func buttonSignUpPhotoSelect(sender: AnyObject) {
        
        // Initalize ALCameraViewController
        let photoSelectController = CameraViewController(croppingEnabled: true) { image in
            print("succcess")
            self.dismissViewControllerAnimated(true, completion: nil)
            
            if let selectedImage = image.0?.CGImage! {
                self.buttonUploadPhoto.setBackgroundImage(UIImage(CGImage: selectedImage), forState: UIControlState.Normal)
            } else {
                print("Default")
            }
        }
        presentViewController(photoSelectController, animated: true, completion: nil)
    }
    
    // Back button action - Go back to Sign up view page 2
    @IBAction func buttonSignUpPhotoBack(sender: AnyObject) {
        viewUploadPhoto.hidden = true
        viewSignUpPage2.hidden = false
    }
    
    // Confirm upload action
    @IBAction func buttonSignUpPhotoConfirmUpload(sender: AnyObject) {
     
     // Setup Firebase
     let storage = FIRStorage.storage()
     
        // Let's setup for uploading the image
        let storageRef = storage.referenceForURL("gs://project-8130163388764754339.appspot.com/resources/profilepics")
        
        // Let's set up the file path as the UId
        FIRAuth.auth()?.addAuthStateDidChangeListener { auth, user in
            if let user = user {
                
                // User logged in
                self.newUrl = user.uid
                print(user.uid)
                
                // Setup data for image
                let imgData: NSData = self.resizeImageForUpload(self.buttonUploadPhoto.backgroundImageForState(UIControlState.Normal)!)
                
                // Create the file metadata
                let metadata = FIRStorageMetadata()
                metadata.contentType = "image/jpeg"
                
                // Upload file and metadata to the object 'newUrl'
                let uploadTask = storageRef.child("\(self.newUrl).jpg").putData(imgData, metadata: metadata)
                
                uploadTask.observeStatus(.Success) { snapshot in
                    // Upload completed successfully
                    print("Upload SUCCESS")
                    
                    let newViewController = self.storyboard?.instantiateViewControllerWithIdentifier("BottomTabBarController") as! BottomTabBarController
                    newViewController.loadView()
                    newViewController.setTabBarColor()
                    self.presentViewController(newViewController, animated: false, completion: nil)
                }
                
            } else {
                print("User is not logged in")
            }
        }


    }
    
    
    @IBAction func buttonConnect(sender: AnyObject) {
        presentLoginView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Check if the user is already signed in
        let handle = FIRAuth.auth()?.addAuthStateDidChangeListener { auth, user in
            if user != nil {
                let newViewController = self.storyboard?.instantiateViewControllerWithIdentifier("BottomTabBarController") as! BottomTabBarController
                newViewController.loadView()
                newViewController.setTabBarColor()
                self.presentViewController(newViewController, animated: false, completion: nil)
            } else {
                print("User not logged in")
            }
            
        }
        
        // Remove the handle soon after the user state is checked
        FIRAuth.auth()?.removeAuthStateDidChangeListener(handle!)
        
        // Assign textfield delegates for catching return keys
        self.textfieldLoginEmail.delegate = self
        self.textfieldLoginPassword.delegate = self
        self.textFieldSignUpEmail.delegate = self
        self.textFieldSignUpPassword.delegate = self
        self.textFieldSignUpPasswordConfirm.delegate = self
        self.textFieldSignUpFirstName.delegate = self
        self.textFieldSignUpLastName.delegate = self
        
        self.hideKeyboardWhenTappedAround()
        
        // Initialize the error code map
        self.errorCodeMap[17007] = "Email already in use"
        self.errorCodeMap[17999] = "Invalid email"
        self.errorCodeMap[17009] = "Invalid credentials"        // Wrong password
        self.errorCodeMap[17011] = "Invalid credentials"        // Account does not exist
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    
    }
    
    // Catch return keys
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if textField == textfieldLoginEmail {
            self.textfieldLoginPassword.becomeFirstResponder()
            
        } else if textField == textfieldLoginPassword {
            self.textfieldLoginPassword.resignFirstResponder()
            self.buttonLogin(self)
            
        } else if textField == textFieldSignUpEmail {
            self.textFieldSignUpPassword.becomeFirstResponder()
            
        } else if textField == textFieldSignUpPassword {
            self.textFieldSignUpPasswordConfirm.becomeFirstResponder()
            
        } else if textField == textFieldSignUpPasswordConfirm {
            self.textFieldSignUpPasswordConfirm.resignFirstResponder()
            self.buttonSignUpPage1Confirm(self)
            
        } else if textField == textFieldSignUpFirstName {
            self.textFieldSignUpLastName.becomeFirstResponder()
            
        } else if textField == textFieldSignUpLastName {
            self.textFieldSignUpLastName.resignFirstResponder()
            self.buttonSignUpPage2Confirm(self)
        }

        return true
    }
    
    func presentLoginView() {
        viewLogIn.hidden = false
        buttonConnect.hidden = true
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        imageViewBackdrop.addSubview(blurEffectView)
    }
    
    
    func presentViewSignUpPage1() {
        viewLogIn.hidden = true
        viewSignUpPage1.hidden = false
        self.dismissKeyboard()
    }
    
    func presentViewSignUpPage2() {
        viewSignUpPage1.hidden = true
        viewSignUpPage2.hidden = false
        self.dismissKeyboard()
    }
    
    func presentViewUploadPhoto() {
        viewSignUpPage2.hidden = true
        viewUploadPhoto.hidden = false
        self.dismissKeyboard()
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Actions
    
    /*
     resizeImage()
     Returns an NSDATA type of a resized UIImage with a max pixel size of 1000.0
     
     Set a new image source to a CGImageSource from a UIImageJPEGRepresentation of image
     Set the options for the new image to
     KCGImageSourceThumbnailMaxPixelSize - 1500.0
     KCGImageSourceCreateThumbnailFromImageAlways - true
     
     Return the NSDATA type of the new resized UIImage
     
     */
    func resizeImageForUpload(image: UIImage) -> NSData {
        
        let imageSource = CGImageSourceCreateWithData(UIImageJPEGRepresentation(image, 1.0)!, nil)
        let options: [NSString: NSObject] = [
            kCGImageSourceThumbnailMaxPixelSize: 700,
            kCGImageSourceCreateThumbnailFromImageAlways: true
        ]
        
        return UIImageJPEGRepresentation(CGImageSourceCreateThumbnailAtIndex(imageSource!, 0, options).flatMap { UIImage(CGImage: $0) }!, 0.8)!
        
    }
    
    /*
     stripTrailingWhitespace()
     This function strips off any trailing whitespace from a string and returns the string back

    */
    func stripTrailingWhitespace(text: String) -> String {
        let stringToStrip = text
        
        // Let's check if the last character in the string is a whitespace
        if stringToStrip[stringToStrip.endIndex.advancedBy(-1)] == " " {
            return stringToStrip.substringToIndex(stringToStrip.endIndex.advancedBy(-1))
        }

        return stringToStrip
    }
    
}



// This allows the keyboard to be dismissed when tapped outside
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}