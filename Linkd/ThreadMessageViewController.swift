//
//  ThreadMessageViewController.swift
//  Linkd
//
//  Created by Antonio Bares on 5/12/16.
//  Copyright © 2016 jav. All rights reserved.
//

import UIKit
import Firebase
import JSQMessagesViewController
import CryptoSwift
import ImageIO

class ThreadMessageViewController: JSQMessagesViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // Variables for messages
    var messages : [Message] = []       // Array to hold all loaded messages
    var messagePos = [UInt : Int]()     // Dictionary to map loaded messages to their positoins
    var messageCounter = 0              // Counts number of messages
    
    var isMedia : Bool = false          // Used to check if message to be sent is a media type message
    
    // Bubble overlays for the messages
    var outoingBubbleImageView = JSQMessagesBubbleImageFactory.init()
    var incomingBubbleImageView = JSQMessagesBubbleImageFactory.init()
    
    // For use with picking image from Photo Library
    let imagePicker = UIImagePickerController()
    var pickedMessageImage : UIImage?
    
    // Thread parameters set by segue
    var threadId : String!
    var threadName: String!
    
    // Setup Firebase
    var currentThreadMessageRef: FIRDatabaseReference!
    var currentThreadLatestTimeStampRef: FIRDatabaseReference!
    
    let storage = FIRStorage.storage()
    
    /*
     setupFirebase()
     Function called in ViewDidLoad()
     Sets up basic Firebase functioning as well as receiving of messages from databae
     
     Set up a current thread reference pointed to the current threadId
     
     Query the last 25 messages from the current thread
     Setup each message's properties
     Check for message type
     
     Text:
     Add the message and position to the messagePos dictionary
     Setup the message and append it to the messages array
     Media:
     Add the message and position to the messagePos dictionary
     Setup a placeholder message and append it to the messages array while we wait for the image to load
     
     Check if the file exists on the local device storage
     
     If the file exists then load it and replace the placeholder message
     Reload the UICollectionView
     If it does not then load it from the server then replace the placeholder message
     Reload the UICollectionView
     
     Play the message received sound if the message sender is not the current sender
     
     */
    
    func setupFirebase() {
        
        self.currentThreadMessageRef = FIRDatabase.database().reference().child("\(Globals.CONSTANTS.THREADS_MESSAGES_ROOT)/\(threadId!)")
        self.currentThreadLatestTimeStampRef = FIRDatabase.database().reference().child("\(Globals.CONSTANTS.THREADS_ROOT)/\(threadId!)/thread-latest-timestamp")
        
        // Receive messages from Firebase (Limit.d to latest 25 messages)
        currentThreadMessageRef.queryLimitedToLast(25).observeEventType(.ChildAdded, withBlock: { (snapshot) in
            let senderName = snapshot.value!.valueForKey("sender-name") as? String
            let senderId = snapshot.value!.valueForKey("sender-id") as? String
            let text = snapshot.value!.valueForKey("text") as? String
            let hash = snapshot.value!.valueForKey("hash") as? UInt
            //let timestamp = snapshot.value.valueForKey("timestamp") as? String
            let isMedia = snapshot.value!.valueForKey("isMedia") as? Bool
            let mediaUrl = snapshot.value!.valueForKey("mediaUrl") as? String
            
            // Check for messageType - Media or Text type
            if isMedia == false {
                // Text type message
                
                // Load hashes and message counts into messagePos for tracking positions
                self.messagePos[hash!] = self.messageCounter
                self.messageCounter += 1
                
                let messageMediaNil = JSQPhotoMediaItem.init()
                let message = Message(senderName: senderName!, senderId: senderId!, text: text!, hash: hash!, isMedia: false, media: messageMediaNil)
                self.messages.append(message)
                
                self.finishReceivingMessage()
            } else {
                
                // Media type message
                self.messagePos[hash!] = self.messageCounter
                self.messageCounter += 1
                // Insert a placeholder message while image is loaded
                let messageMediaNil = JSQPhotoMediaItem.init()
                let message = Message(senderName: senderName!, senderId: senderId!, text: text!, hash: hash!, isMedia: true, media: messageMediaNil)
                self.messages.append(message)
                
                // Get the local device storage Url for file
                var imgUrl = (NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)).last! as NSURL
                imgUrl = imgUrl.URLByAppendingPathComponent( "images/\(mediaUrl!).jpg")
                
                // Check if media item already exsists in local device storage
                var errorType : NSError?
                let fileIsAvailable =  imgUrl.checkResourceIsReachableAndReturnError(&errorType)
                
                // Run the file check
                if fileIsAvailable == true {
                    // File is available so let's load it from the local storage
                    
                    let mediaItem = JSQPhotoMediaItem(image: self.resizeImage(imgUrl))
                    
                    let message = Message(senderName: self.senderDisplayName, senderId: self.senderId, text: text!, hash: hash!, isMedia: true, media: mediaItem)
                    
                    // Debuggers
                    print(self.messages.count)
                    print(self.messagePos[hash!]!)
                    
                    // Since we loaded placeholders for the images, let's remove them and replace them with the newly loaded images
                    self.messages.insert(message, atIndex: self.messagePos[hash!]!)
                    self.messages.removeAtIndex(self.messagePos[hash!]! + 1)
                    
                    self.finishReceivingMessage()
                    self.collectionView.reloadData()
                    
                } else {
                    // File is not available so let's download it from the corresponding url and store it in the local device storage
                    let storageRef = self.storage.referenceForURL("gs://project-8130163388764754339.appspot.com/images")
                    let imageRef = storageRef.child("\(mediaUrl!).jpg")
                    
                    // Debuggers
                    print(imageRef.fullPath)
                    print(imgUrl.absoluteString)
                    
                    // Begin downloading task
                    let downloadTask = imageRef.writeToFile(imgUrl) { (URL, error) -> Void in
                        if (error != nil) {
                            print(error)
                        } else {
                            // Download was succcessful so let's load the file from the local storage
                            print("success")
                            
                            let mediaItem = JSQPhotoMediaItem(image: self.resizeImage(imgUrl))
                            let message = Message(senderName: self.senderDisplayName, senderId: self.senderId, text: text!, hash: hash!, isMedia: true, media: mediaItem)
                            
                            // Debuggers
                            print(self.messages.count)
                            print(self.messagePos[hash!]!)
                            
                            // Since we loaded placeholders for the images, let's remove them and replace them with the newly loaded images
                            self.messages.insert(message, atIndex: self.messagePos[hash!]!)
                            self.messages.removeAtIndex(self.messagePos[hash!]! + 1)
                            
                            
                            self.finishReceivingMessage()
                            self.collectionView.reloadData()
                        }
                        
                        
                    }
                    
                    
                }
                
                
            }
            
            // Play message receieved sound as long as the message is from a different sender
            if senderId! != self.senderId {
                JSQSystemSoundPlayer.jsq_playMessageReceivedSound()
            }
            
            
        })
        
        
    }

    /*
     sendMessage()
     Function is called when the send button is pressed
     
     Create a unique hash for the message
     Check the message's type
     
     media:
     Setup the storage reference
     Create a new Url for the file and check if it is taken already
     
     Setup the message and image data
     Upload the image to the server then send the message to the database
     
     Change isMedia back to false
     
     Text:
     Setup the message
     Send the message to the databse
     
     Play message sent sound
     
     */
    
    // Add a text message to Firebase
    func sendMessage(senderName: String, senderId: String, text: String) {
        
        // Create a hash for the message
        let newDate = NSDate().timeIntervalSince1970 / 10
        let randomVal = arc4random_uniform(10000)
        let hash = UInt(newDate / Double(randomVal))
        
        // Check if the message will be a media type
        if self.isMedia == true {
            
            let storageRef = storage.referenceForURL("gs://project-8130163388764754339.appspot.com/images")
            
            // Let's randomize a string for the file
            var newUrl = randomString(35)
            let mediaRef = storageRef.child("\(newUrl).jpg")
            
            // Check if the url is already taken
            mediaRef.metadataWithCompletion { (metadata, error) -> Void in
                
                // If it returns error then the url must not be taken yet if not then make another url
                if (error != nil) {
                    print("Url is good")
                } else {
                    print("Url is not good. Making new url...")
                    newUrl = self.randomString(35)
                }
                
            }
            
            // Set up the message for sending
            let newMessage = [
                "sender-name": senderName,
                "sender-id": senderId,
                "text": text,
                "hash": hash,
                "isMedia": true,
                "mediaUrl": newUrl
            ]
            
            // Setup data for image
            let imgData: NSData = self.resizeImageForUpload(pickedMessageImage!)
            
            // Create the file metadata
            let metadata = FIRStorageMetadata()
            metadata.contentType = "image/jpeg"
            
            // Upload file and metadata to the object 'newUrl'
            let uploadTask = storageRef.child("\(newUrl).jpg").putData(imgData, metadata: metadata)
            
            uploadTask.observeStatus(.Success) { snapshot in
                // Upload completed successfully
                print("Upload SUCCESS")
                
                // Insert the message into the database
                self.currentThreadMessageRef.childByAutoId().setValue(newMessage)
                
                // Create a new latest timestamp for the thread as a string
                //let newTimestampAsString = Int(NSDate().timeIntervalSince1970 * -1)
                let newTimestampAsString = Int(NSDate().timeIntervalSince1970)
                
                // Update the thread's latest timestamp
                self.currentThreadLatestTimeStampRef.setValue(newTimestampAsString)
            }
            
            // TextView should not have media in it anymore so let's set isMedia to false
            self.isMedia = false
        }
        else {
            // Text type message
            let newMessage = [
                "sender-name": senderName,
                "sender-id": senderId,
                "text": text,
                "hash": hash,
                "isMedia": false,
                "mediaUrl": ""
                
                //"timestamp": NSDate().timeIntervalSince1970
                
            ]
            // Insert the message into the database
            currentThreadMessageRef.childByAutoId().setValue(newMessage)
            
            // Create a new latest timestamp for the thread as a string
            //let newTimestampAsString = Int(NSDate().timeIntervalSince1970 * -1)
            let newTimestampAsString = Int(NSDate().timeIntervalSince1970)
            
            // Update the thread's latest timestamp
            self.currentThreadLatestTimeStampRef.setValue(newTimestampAsString)
        }
        
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        imagePicker.delegate = self
        
        setupFirebase()
        
        self.navigationItem.title = threadName!
        self.tabBarController?.tabBar.hidden = true
        
        // Reference for Threads List
        let threadsRef = FIRDatabase.database().reference().child(Globals.CONSTANTS.THREADS_ROOT)
        let currentThreadRef = threadsRef.child("\(threadId!)/thread-name")

        currentThreadRef.observeEventType(.ChildChanged, withBlock: {(snapshot) in
            print("...")
            print(snapshot)
            
            //self.navigationItem.title = snapshot.value as! String
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    // MARK - Data Source
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.messages.count
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageData! {
        let data = self.messages[indexPath.row]
        return data
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, didDeleteMessageAtIndexPath indexPath: NSIndexPath!) {
        self.messages.removeAtIndex(indexPath.row)
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item]
        
        if message.senderId() == senderId {
            return JSQMessagesBubbleImageFactory.init().outgoingMessagesBubbleImageWithColor(UIColor(red: 0.7529, green: 0.2235, blue: 0.1686, alpha: 1.0) )
        }
        return JSQMessagesBubbleImageFactory.init().incomingMessagesBubbleImageWithColor(UIColor(red: 0.9255, green: 0.3922, blue: 0.2941, alpha: 1.0) )
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAtIndexPath indexPath: NSIndexPath!) -> NSAttributedString! {
        let data = self.collectionView(self.collectionView, messageDataForItemAtIndexPath: indexPath)
        if (self.senderDisplayName == data.senderDisplayName()) {
            return  nil
        }
        return NSAttributedString(string: data.senderDisplayName())
    }
    
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        let data = self.collectionView(self.collectionView, messageDataForItemAtIndexPath: indexPath)
        if (self.senderDisplayName == data.senderDisplayName()) {
            return 0.0
        }
        return kJSQMessagesCollectionViewCellLabelHeightDefault
    }
    
    
    // MARK -- Actions
    
    
    /*
     didPressSendButton()
     Function is called whenever the send button is pressed
     
     Play message sent sound
     Send message out
     
     */
    override func didPressSendButton(button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: NSDate!) {
        
        sendMessage(senderDisplayName, senderId: senderId, text: text)
        finishSendingMessage()
    }
    
    /*
     didPressAccessoryButton()
     Function is called whenever the paperclip button is pressed
     
     Setup UIImagePickerController
     Don't allow editing
     Allow user to pick from Photo Library
     *In the future, allow camera to be used*
     
     Add linebreaks to the UITextView to make room for selected image
     
     Present the UIImagePickerController
     
     */
    override func didPressAccessoryButton(sender: UIButton!) {
        
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
        
        let attributedString = NSMutableAttributedString(string: "\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
        
        self.inputToolbar.contentView.textView.attributedText = attributedString
        
        presentViewController(imagePicker, animated: true, completion: nil)
        
    }
    
    
    /*
     imagePickerController()
     Function is called when UIImagePickerController, imagePicker, is presented
     
     Get the picked image and set it to newImage
     Scale down the image in order to fit it properly inside the UITextView
     Set the attributedText of the UITextView to the newImage along with a linebreak
     
     Fix the font of the UITextView back to System Font with 16Pt size
     Enable the send button
     
     Dismiss the UIImagePickerController
     
     Set the UI focus to the UITextView and the cursor to the position after the newImage
     
     Set the selectedImage to the newImage
     Set isMedia to true
     
     */
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        // Set newImage to the picked image
        var newImage: UIImage
        if let possibleImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            newImage = possibleImage
        } else if let possibleImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            newImage = possibleImage
        } else {
            return
        }
        
        self.pickedMessageImage = newImage
        
        // Setup the image and a linebreak to be inside the UITextView
        let textAttachment = NSTextAttachment()
        let attributedString = NSMutableAttributedString(string: "\n")
        textAttachment.image = newImage
        
        let oldWidth = textAttachment.image!.size.width;
        let scaleFactor = oldWidth / (self.inputToolbar.contentView.textView.frame.size.width - 30);
        textAttachment.image = UIImage(CGImage: textAttachment.image!.CGImage!, scale: scaleFactor, orientation: .Up)
        attributedString.replaceCharactersInRange(NSMakeRange(0, 1), withAttributedString: NSAttributedString(attachment: textAttachment))
        self.inputToolbar.contentView.textView.attributedText = attributedString
        
        // Since we changed the attributedText we need to fix the font back
        self.inputToolbar.contentView.textView.font = UIFont.systemFontOfSize(16.0)
        
        // Now let's enable the send button
        self.inputToolbar.contentView.rightBarButtonItem.enabled = true
        
        dismissViewControllerAnimated(true, completion: nil)
        
        // Set the UI focus to the UITextView and move the cursor next to the image
        self.inputToolbar.contentView.textView.becomeFirstResponder()
        
        var arbitraryValue: Int = 0
        if let newPosition = self.inputToolbar.contentView.textView.positionFromPosition(self.inputToolbar.contentView.textView.beginningOfDocument, inDirection: UITextLayoutDirection.Right, offset: arbitraryValue) {
            
            self.inputToolbar.contentView.textView.selectedTextRange = self.inputToolbar.contentView.textView.textRangeFromPosition(newPosition, toPosition: newPosition)
        }
        arbitraryValue = 1
        
        if let newPosition = self.inputToolbar.contentView.textView.positionFromPosition(self.inputToolbar.contentView.textView.beginningOfDocument, inDirection: UITextLayoutDirection.Right, offset: arbitraryValue) {
            
            self.inputToolbar.contentView.textView.selectedTextRange = self.inputToolbar.contentView.textView.textRangeFromPosition(newPosition, toPosition: newPosition)
        }
        
        // Since we have inserted an image let's set isMedia to true
        self.isMedia = true
    }
    
    
    /*
     imagePickerControllerDidCancel()
     Function is called whenever user hits cancel when the imagePicker is presented
     
     Dismiss UIImagePickerController
     
     */
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    /*
     randomString()
     Returns a random string of size 'length'
     
     Create a string of allowed characters
     Create a string to hold the randomized string
     
     Loop through the size 'length'
     Create a random number with a range of the allowed characters
     Create a new chacteracter by accessing the allowed characters using an index incremented by the random number
     Concatenate the new character to the randomized string
     
     Return the randomized string
     
     */
    func randomString(length: Int) -> String {
        
        let allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-"
        let allowedCharsCount = UInt32(allowedChars.characters.count)
        var randomString = ""
        
        for _ in (0..<length) {
            let randomNum = Int(arc4random_uniform(allowedCharsCount))
            let newCharacter = allowedChars[allowedChars.startIndex.advancedBy(randomNum)]
            randomString += String(newCharacter)
        }
        
        return randomString
    }
    
    
    /*
     resizeImage()
     Returns a resized UIImage with a max pixel size of 1000.0
     
     Set a new image source to a CGImageSource using an NSURL
     Set the options for the new image to
     KCGImageSourceThumbnailMaxPixelSize - 1000.0
     KCGImageSourceCreateThumbnailFromImageAlways - true
     
     Return the new resized UIImage
     
     */
    func resizeImage(imgUrl: NSURL) -> UIImage {
        
        let imageSource = CGImageSourceCreateWithURL(imgUrl, nil)
        let options: [NSString: NSObject] = [
            kCGImageSourceThumbnailMaxPixelSize: 1000.0,
            kCGImageSourceCreateThumbnailFromImageAlways: true
        ]
        
        return CGImageSourceCreateThumbnailAtIndex(imageSource!, 0, options).flatMap { UIImage(CGImage: $0) }!
        
    }
    
    
    /*
     resizeImage()
     Returns an NSDATA type of a resized UIImage with a max pixel size of 1000.0
     
     Set a new image source to a CGImageSource from a UIImageJPEGRepresentation of image
     Set the options for the new image to
     KCGImageSourceThumbnailMaxPixelSize - 1500.0
     KCGImageSourceCreateThumbnailFromImageAlways - true
     
     Return the NSDATA type of the new resized UIImage
     
     */
    func resizeImageForUpload(image: UIImage) -> NSData {
        
        let imageSource = CGImageSourceCreateWithData(UIImageJPEGRepresentation(image, 1.0)!, nil)
        let options: [NSString: NSObject] = [
            kCGImageSourceThumbnailMaxPixelSize: 1500.0,
            kCGImageSourceCreateThumbnailFromImageAlways: true
        ]
        
        return UIImageJPEGRepresentation(CGImageSourceCreateThumbnailAtIndex(imageSource!, 0, options).flatMap { UIImage(CGImage: $0) }!, 0.8)!
        
    }

     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        
        if segue.identifier == "segueThreadMessageSettings" {
            let ThreadMessageSettingsTableVC = segue.destinationViewController as! ThreadMessageSettingsTableViewController
            
            ThreadMessageSettingsTableVC.threadName = self.threadName
            
            
        }
     }
    
    // MARK: - Unwind segue
    @IBAction func changeThreadMessageName(segue: UIStoryboardSegue) {
        let ThreadMessageSettingsVC = segue.sourceViewController as! ThreadMessageSettingsTableViewController
        
        let newThreadName = ThreadMessageSettingsVC.textFieldThreadName.text!
        
        let currentThreadNameRef = FIRDatabase.database().reference().child("\(Globals.CONSTANTS.THREADS_ROOT)/\(threadId!)/thread-name")
        
        currentThreadNameRef.setValue(newThreadName)
        
        self.navigationItem.title = newThreadName
    }
    

    
}
