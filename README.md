# Linkd -- Let's Connect

![](https://gitlab.com/agbares/Linkd/raw/master/Media/icon.png)

---

Linkd is a social networking application built for the iOS platform.  
Linkd was a group final project for a class, Intro to Engineering, at De Anza College.